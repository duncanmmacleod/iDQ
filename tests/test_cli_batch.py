__description__ = "a module that tests several aspects of the batch CLI"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])


def test_cli_batch(script_runner):
    ret = script_runner.run("idq-batch", "--help")
    assert ret.success


def test_cli_calibrate(script_runner):
    ret = script_runner.run("idq-calibrate", "--help")
    assert ret.success


def test_cli_evaluate(script_runner):
    ret = script_runner.run("idq-evaluate", "--help")
    assert ret.success


def test_cli_timeseries(script_runner):
    ret = script_runner.run("idq-timeseries", "--help")
    assert ret.success


def test_cli_train(script_runner):
    ret = script_runner.run("idq-train", "--help")
    assert ret.success


def test_cli_report(script_runner):
    ret = script_runner.run("idq-report", "--help")
    assert ret.success
