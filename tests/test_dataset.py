__description__ = "a module that tests several aspects of the Dataset class"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#--------------------------------------------------------------------------

import sys
import pytest

import numpy as np

from idq import configparser
from idq import factories
from idq import utils

#--------------------------------------------------------------------------

#----------------------
#        tests
#----------------------

@pytest.mark.usefixtures('snax_dataset')
class TestLabeledDataset(object):
    """
    Tests several features for a labeled Dataset.
    """
    def test_times(self, snax_dataset_conf):
        assert len(self.labeled_dataset) == (len(self.target_times) + len(self.random_times)), 'dataset not the right size'
        assert self.labeled_dataset.segs == self.dataloader.segs, 'dataset segs not set properly'
        assert self.labeled_dataset.start == self.dataloader.start, 'dataset start time not set properly'
        assert self.labeled_dataset.end == self.dataloader.end, 'dataset end time not set properly'

    def test_validate(self, snax_dataset_conf):
        self.labeled_dataset.validate()

    def test_vectors2classes(self, snax_dataset_conf):
        glitch, clean = self.labeled_dataset.vectors2classes()

        assert len(glitch) == len(self.target_times), 'wrong number of glitch samples in dataset'
        assert len(clean) == len(self.random_times), 'wrong number of clean samples in dataset'

        for vector in glitch:
            assert vector.label == 1, 'wrong label for glitch sample'
        for vector in clean:
            assert vector.label == 0, 'wrong label for clean sample'

    def test_vectors2num(self, snax_dataset_conf):
        num_glitch, num_clean = self.labeled_dataset.vectors2num()
        assert num_glitch == len(self.target_times), 'dataset does not have the right number of glitch samples'
        assert num_clean == len(self.random_times), 'dataset does not have the right number of clean samples'

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_remove_vectors(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_copy(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="need numpy 1.9.0+ to use np.unique with return_counts kwarg")
    #def test_labels(self, snax_dataset_conf):
    #    unique, counts = np.unique(self.labeled_dataset.labels, return_counts=True)
    #    counter = dict(zip(unique, counts))

    #    assert set(counter.keys()) == set([0, 1]), 'more labels in dataset than allowed'
    #    assert len(counter[0]) == len(self.random_times), 'dataset.labels does not have the right number of clean samples'
    #    assert len(counter[1]) == len(self.target_times), 'dataset.labels does not have the right number of clean samples'

    def test_ranks(self, snax_dataset_conf):
        assert np.isnan(self.labeled_dataset.ranks).all(), 'dataset.ranks contain a quantity other than None'

    def test_times(self, snax_dataset_conf):
        times = np.concatenate((self.target_times, self.random_times)).sort()
        ref_times = np.array(self.labeled_dataset.times).sort()
        assert np.array_equal(ref_times, times), 'gps times of dataset do not match times used to generate dataset'

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_apply_ranks(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_append_dataset(self, snax_dataset_conf):
    #    raise NotImplementedError

    def test_vectorize(self, snax_dataset_conf):
        result = self.labeled_dataset.features

@pytest.mark.usefixtures('snax_dataset')
class TestUnlabeledDataset(object):
    """
    Tests several features for an unlabeled Dataset.
    """
    def test_properties(self, snax_dataset_conf):
        assert len(self.unlabeled_dataset) == (utils.livetime(self.dataloader.segs) / 2.)
        assert self.unlabeled_dataset.segs == self.dataloader.segs, 'dataset segs not set properly'
        assert self.unlabeled_dataset.start == self.dataloader.start, 'dataset start time not set properly'
        assert self.unlabeled_dataset.end == self.dataloader.end, 'dataset end time not set properly'

        for vector in self.unlabeled_dataset:
            assert np.isnan(vector.label), 'feature vector should be unlabeled'
            assert np.isnan(vector.rank), 'feature vector should be unranked'

    def test_validate(self, snax_dataset_conf):
        self.unlabeled_dataset.validate()

    def test_vectors2classes(self, snax_dataset_conf):
        glitch, clean = self.unlabeled_dataset.vectors2classes()

        assert len(glitch) == 0, 'glitch samples present in unlabeled dataset'
        assert len(clean) == 0, 'samples present in unlabeled dataset'

    def test_vectors2num(self, snax_dataset_conf):
        num_glitch, num_clean = self.unlabeled_dataset.vectors2num()
        print(num_glitch, num_clean)

        assert num_glitch == 0, 'glitch samples present in unlabeled dataset'
        assert num_clean == 0, 'samples present in unlabeled dataset'

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_remove_vectors(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_copy(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_labels(self, snax_dataset_conf):
    #    raise NotImplementedError

    def test_ranks(self, snax_dataset_conf):
        assert np.isnan(self.unlabeled_dataset.ranks).all(), 'dataset.ranks contain a quantity other than None'

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_gpss(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_apply_ranks(self, snax_dataset_conf):
    #    raise NotImplementedError

    #@pytest.mark.skip(reason="not implemented yet")
    #def test_append_dataset(self, snax_dataset_conf):
    #    raise NotImplementedError

    def test_vectorize(self, snax_dataset_conf):
        result = self.unlabeled_dataset.features
