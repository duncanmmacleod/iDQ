#!/usr/bin/env python

__usage__ = "sanitycheck_calibrate-coverage [--options]"
__doc__ = "basic sanity check for idq-calibrate"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os
import sys

import numpy as np
from scipy.stats import norm
from scipy.stats import beta

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from ConfigParser import SafeConfigParser

from optparse import OptionParser

### non-standard libraries
from idq import calibration
erf = calibration.erf

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('', '--size', default=1000, type='int')
parser.add_option('', '--Ntrial', default=50, type='int')
parser.add_option('', '--num_points', default=501, type='int')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')

opts, args = parser.parse_args()

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

if opts.tag:
    opts.tag = "_"+opts.tag

#-------------------------------------------------

### generate plots for a few random distributions and check accuracy, coverage, etc

for distrib, name, params in [(norm, 'gaussian', (0.5, 0.1)), (beta, 'beta', (2, 5))]:

    # gaussian centered on 0.5 with std~0.1
    if opts.verbose:
        print('checking coverage with '+name)

    # find an appropriate bandwidth, basic set-up
    if opts.verbose:
        print('setting up basic things')
    kde = calibration.FixedBandwidth1DKDE(distrib.rvs(*params, size=opts.size), num_points=opts.num_points, compute=False)
    kde.optimize()
    kde.compute()

    b = kde.b
    x = kde.interp_x
    dx = x[1]-x[0]

    # find error bars for the true distribution
    pdf = distrib.pdf(x, *params)
    Ep = []
    Vp = []
    Ec = []
    Vc = []
    for X in x:
        s = 1./(b*2**0.5)
        z = 0.5*(erf((1-x)*s) - erf(-x*s) + erf(s*(1+x)) - erf(s*x) + erf((1-2+x)*s) - erf((-2+x)*s))
        count = np.trapz(pdf*z, dx=dx)

        z = 0.5*(erf(s*(X-x)) - erf(-x*s) + erf(s*(X+x)) - erf(s*x) + erf((X-2+x)*s) - erf((-2+x)*s))
        Ec.append( np.trapz(pdf*z, dx=dx)/count )
        Vc.append( (np.trapz(pdf*z**2, dx=dx)/count-Ec[-1]**2)/opts.size )

        s = 1./(2*b**2)
        z = (np.exp(-s*(X-x)**2) + np.exp(-s*(X+x)**2) + np.exp(-s*(X-2+x)**2)) / 3
        Ep.append( np.trapz(pdf*z, dx=dx)/count )
        Vp.append( (np.trapz(pdf*z**2, dx=dx)/count - Ep[-1]**2)/opts.size )

    Ep = np.array(Ep)
    Vp = np.array(Vp)
    Ec = np.array(Ec)
    Vc = np.array(Vc)

    kde._interp_pdf_alpha, kde._interp_pdf_beta = kde._cumulants2params(Ep, Vp)
    kde._interp_cdf_alpha, kde._interp_cdf_beta = kde._cumulants2params(Ec, Vc)

    scale = 3./(2*np.pi*kde.b**2)**0.5
    kde._interp_logpdf = np.log(Ep) + np.log(scale)
    kde._interp_cdf = Ec

    Epdf = kde.pdf(x)
    Ecdf = kde.cdf(x)

    #---------------------------------------------

    if opts.verbose:
        print('setting up plot')

    ### compute fraction of time KDE from random draw falls within errors derived from true distrib
    fig = plt.figure() # figure for pdf coverage
    ax_1 = plt.subplot(2,3,1)
    ax_2 = plt.subplot(2,3,4)
    ax_3 = plt.subplot(1,3,2)
    ax_4 = plt.subplot(1,3,3)

    FIG = plt.figure() # figure for cdf coverage
    AX_1 = plt.subplot(2,3,1)
    AX_2 = plt.subplot(2,3,4)
    AX_3 = plt.subplot(1,3,2)
    AX_4 = plt.subplot(1,3,3)

    # plot some basic stuff first that describes true distrib
    for q in [0.1, 0.2, 0.3, 0.4]:
        lq = kde.pdf_quantile(x, q)
        hq = kde.pdf_quantile(x, 1.-q)

        ax_1.fill_between(x, lq, hq, color='k', alpha=0.10)
        ax_2.fill_between(x, lq-Epdf, hq-Epdf, color='k', alpha=0.10)

        lq = kde.cdf_quantile(x, q)
        hq = kde.cdf_quantile(x, 1.-q)

        AX_1.fill_between(x, lq, hq, color='k', alpha=0.10)
        AX_2.fill_between(x, lq-Ecdf, hq-Ecdf, color='k', alpha=0.10)

    ax_1.plot(x, Epdf, 'k')
    ax_2.plot(x, np.zeros_like(Epdf), 'k')

    AX_1.plot(x, Ecdf, 'k')
    AX_2.plot(x, np.zeros_like(Ecdf), 'k')

    p = np.linspace(0, 1, 501)
    s = ((1-p)*p/opts.Ntrial)**0.5
    for ax in [ax_3, ax_4, AX_3, AX_4]:
        ax.fill_between(p, p+s, p-s, color='k', alpha=0.25)
        ax.plot(p, p, 'k')

    # now sample
    if opts.verbose:
        print('sampling %d times and computing coverage'%opts.Ntrial)
    ans = np.empty((opts.num_points, opts.Ntrial), dtype='float') ### pdf coverage with errors from known distrib
    sna = np.empty((opts.num_points, opts.Ntrial), dtype='float') ### pdf coverage with errors from KDE realizations

    ANS = np.empty((opts.num_points-3, opts.Ntrial), dtype='float') ### cdf coverage with errors from known distrib
    SNA = np.empty((opts.num_points-3, opts.Ntrial), dtype='float') ### cdf coverage with errors from KDE realizations
    for i in range(opts.Ntrial):
        if opts.verbose:
            sys.stdout.write('\r%d / %d'%(i+1, opts.Ntrial))
            sys.stdout.flush()

        k = calibration.FixedBandwidth1DKDE(distrib.rvs(*params, size=opts.size), num_points=opts.num_points, b=kde.b)

        # plot basic traces
        _pdf = k.pdf(x)
        ax_1.plot(x, _pdf, alpha=0.10)
        ax_2.plot(x, _pdf-Epdf, alpha=0.10)

        _cdf = k.cdf(x)
        AX_1.plot(x, _cdf, alpha=0.10)
        AX_2.plot(x, _cdf-Ecdf, alpha=0.10)

        # compute cdf as a function of x

        #--- pvalues for observing KDE given error estimates from known distribution
        ans[:,i] = [beta.cdf(*args) for args in zip(_pdf/scale, kde.pdf_alpha(x), kde.pdf_beta(x))]

        #--- pvalues for known distribution given error estimates from KDE
        sna[:,i] = [beta.cdf(*args) for args in zip(Epdf/scale, k.pdf_alpha(x), k.pdf_beta(x))]

        #--- repeat for cumulative distributions
        ANS[:,i] = [beta.cdf(*args) for args in zip(_cdf[1:-2], kde.cdf_alpha(x[1:-2]), kde.cdf_beta(x[1:-2]))]
        SNA[:,i] = [beta.cdf(*args) for args in zip(Ecdf[1:-2], k.cdf_alpha(x[1:-2]), k.cdf_beta(x[1:-2]))]

    if opts.verbose:
        sys.stdout.write('\n')

    # now plot coverage for each x separate
    if opts.verbose:
        print('computing coverage')
    bins = np.linspace(0,1,501)
    weights = np.ones((opts.num_points, opts.Ntrial), dtype='float')/opts.Ntrial
    m = np.max(Epdf)
    for i in range(opts.num_points):
        if opts.verbose:
            sys.stdout.write('\r%d / %d'%(i+1, opts.num_points))
            sys.stdout.flush()

        alpha = 0.25*Epdf[i]/m

        truth = ans[i,:]==ans[i,:] ### identify nans
        ax_3.hist(ans[i,truth], bins=bins, histtype='step', cumulative=True, weights=weights[i,truth], color='r', alpha=alpha)

        truth = sna[i,:]==sna[i,:]
        ax_4.hist(sna[i,truth], bins=bins, histtype='step', cumulative=True, weights=weights[i,truth], color='r', alpha=alpha)

        if i>0 and i<(opts.num_points-3): ### skip end points because we know errors are bad there due to fixed boundary conditions
            truth = ANS[i,:]==ANS[i,:] ### identify nans
            AX_3.hist(ANS[i,truth], bins=bins, histtype='step', cumulative=True, weights=weights[i,truth], color='r', alpha=alpha)

            truth = SNA[i,:]==SNA[i,:]
            AX_4.hist(SNA[i,truth], bins=bins, histtype='step', cumulative=True, weights=weights[i,truth], color='r', alpha=alpha)

        weights[i,:] *= Epdf[i]

    weights /= np.sum(weights)

    if opts.verbose:
        sys.stdout.write('\n')

    # plot averaged coverage
    pdfweights = weights.flatten()
    ans = ans.flatten()
    truth = ans==ans
    ax_3.hist(ans[truth], bins=bins, histtype='step', cumulative=True, weights=pdfweights[truth]/np.sum(pdfweights[truth]), color='b', alpha=1.0)

    sna = sna.flatten()
    truth = sna==sna
    ax_4.hist(sna[truth], bins=bins, histtype='step', cumulative=True, weights=pdfweights[truth]/np.sum(pdfweights[truth]), color='b', alpha=1.0)

    cdfweights = weights[1:-2,:].flatten()
    ANS = ANS.flatten()
    truth = ANS==ANS
    AX_3.hist(ANS[truth], bins=bins, histtype='step', cumulative=True, weights=cdfweights[truth]/np.sum(cdfweights[truth]), color='b', alpha=1.0)

    SNA = SNA.flatten()
    truth = SNA==SNA
    AX_4.hist(SNA[truth], bins=bins, histtype='step', cumulative=True, weights=cdfweights[truth]/np.sum(cdfweights[truth]), color='b', alpha=1.0)

    # decorate

    for ax in [ax_1, ax_2, ax_3, ax_4, AX_1, AX_2, AX_3, AX_4]:
        ax.set_xlim(xmin=0, xmax=1)
    ax_1.set_ylim(ymin=0)
    AX_1.set_ylim(ymin=0, ymax=1)

    for ax in [ax_3, ax_4, AX_3, AX_4]:
        ax.set_ylim(ymin=0, ymax=1)
        ax.set_xlabel('nominal p-value')

    for ax in [ax_1, AX_1]:
        plt.setp(ax.get_xticklabels(), visible=False)
    for ax in [ax_3, AX_3]:
        plt.setp(ax.get_yticklabels(), visible=False)

    ax_1.set_ylabel('smoothed pdf')
    AX_1.set_ylabel('smoothed cdf')

    for ax in [ax_2, AX_2]:
        ax.set_ylabel('true - kde')
        ax.set_xlabel('rank')

    for ax in [ax_4, AX_4]:
        ax.set_ylabel('observed fraction')
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')

    for ax in [ax_3, AX_3]:
        ax.text(0, 1, 'pvalue of\n    KDE\ngiven errors from\n    true distrib', ha='left', va='top', fontsize=10)

    for ax in [ax_4, AX_4]:
        ax.text(0, 1, 'pvalue of\n    true distrib\ngiven errors from\n    KDE', ha='left', va='top', fontsize=10)

    plt.subplots_adjust(hspace=0.05, wspace=0.05, top=0.95)

    figname = os.path.join(opts.output_dir, 'sanitycheck_calibrate-coverage_pdf-%s%s.png'%(name, opts.tag))
    if opts.verbose:
        print(figname)
    fig.savefig(figname)
    plt.close(fig)

    FIGNAME = os.path.join(opts.output_dir, 'sanitycheck_calibrate-coverage_cdf-%s%s.png'%(name, opts.tag))
    if opts.verbose:
        print(FIGNAME)
    FIG.savefig(FIGNAME)
    plt.close(FIG)
