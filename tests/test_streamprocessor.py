__description__ = "a module that tests several aspects of StreamProcessor"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#--------------------------------------------------------------------------

import sys
import pytest

import numpy as np

from ligo import segments as ligo_segments

from idq import utils

#--------------------------------------------------------------------------

#----------------------
#        tests
#----------------------

@pytest.mark.usefixtures('stream_processor')
class TestStreamProcessor(object):
    """
    Tests several aspects of StreamProcessor.
    """

    def test_timestamp(self, kafka_dataloader_conf):
        stream = self.stream_processor
        data, segs = stream.poll()
        assert data.segs in segs.coalesce(), 'span of data in DataChunk is inconsistent with timestamps'

    def test_poll(self, kafka_dataloader_conf):
        stream = self.stream_processor
        t_start = stream.timestamp
        data, _ = stream.poll()
        while not data.features:
            data, _ = stream.poll()

        assert set(data.channels) == set(kafka_dataloader_conf['channels']), 'incorrect channels stored in DataChunk after polling'

    def test_flush(self, kafka_dataloader_conf, retain=1):
        stream = self.stream_processor
        data, segs = stream.poll()
        while not data.features:
            timestamp = stream.timestamp
            data, segs = stream.poll()

        timestamp = segs[0][0]

        stream.flush(retain=retain)
        assert len(stream._queue) == retain, 'should only be one DataLoader object in Umbrella after flushing stream'
        assert stream._queue[0].start >= timestamp, 'buffer stored in Umbrella should have a start time at least as large as the current timestamp'
        assert stream._buffer.start >= timestamp, 'buffer stored in StreamProcessor should have a start time greater than or equal to timestamp'
