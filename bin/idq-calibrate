#!/usr/bin/env python

__usage__ = "idq-calibrate [--options] config.ini gps_start gps_end"
__description__ = "an executable to generate calibrated classifiers. This is meant as a one-off batch job, not a streaming process."
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#---------------------------------------------------------------------------------------------------

import os
import sys
import traceback
from optparse import OptionParser

### dependencies not in standard Python libraries
from idq import batch
from idq import configparser
from idq import logs
from idq import names
from idq import utils

#---------------------------------------------------------------------------------------------------

### parser arguments
parser = OptionParser(usage=__usage__, description=__description__)

parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print to stdout in addition to writing to automatically generated log')

parser.add_option('-p', '--preferred', default=False, action='store_true',
    help='choose whether to search for a preferred quiver. DEFAULT=False.')

opts, args = parser.parse_args()

assert len(args)==3, 'please supply exactly 3 input arguments\n%s'%__usage__
gps_start, gps_end = [float(_) for _ in args[1:]]
assert gps_end > gps_start, "gps_end (%.3f) must be > gps_start (%.3f)!"%(gps_end, gps_start)

config_path = args[0]

#-------------------------------------------------

try: 
    batch.calibrate(gps_start, gps_end, config_path, **vars(opts))

except Exception as e: ### allow us to clean up forked jobs if needed (no zombies for us!)
    trcbck = traceback.format_exc().strip("\n") ### extract traceback

    ### print to log for safe-keeping
    config = configparser.path2config(config_path)
    tag = config.tag
    logger = logs.configure_logger(
        batch.logger,
        names.tag2calibrate_logname(tag),
        log_level=config.calibrate['log_level'],
        rootdir=names.tag2logdir(tag, rootdir=os.path.abspath(config.rootdir)),
        verbose=opts.verbose,
    )
    logger.error(trcbck)

    sys.exit(1)
