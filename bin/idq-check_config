#!/usr/bin/env python

__usage__ = "idq-check_config [--options] config.ini"
__description__ = "an executable to generate trained classifiers"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#---------------------------------------------------------------------------------------------------

import traceback
from optparse import OptionParser

### dependencies not in standard Python libraries
from idq import configparser
from idq import utils
    
#---------------------------------------------------------------------------------------------------

def check_config(config_path, verbose=False):
    '''
    sanity check the configuration values
    '''
    raise NotImplementedError('''\
 - all classifiers in the general section have a section of their own
 - train data discovery has columns that match target_bounds
 - general section has the following options
    - tag
    - rootdir
 - default values are discoverable
    - workflow
    - log_level
 - all required kwargs are present when we try to build objects
    - classifier_data
    - classifier
 - workflow is one of (block, fork, condor)
 - condor section has
    - universe
    - retry
    - accounting_group
    - acconting_group_user
''')

#---------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    ### parse arguments
    parser = OptionParser(usage=__usage__, description=__description__)

    parser.add_option('-v', '--verbose', default=False, action='store_true',
        help='print to results to stdout')

    opts, args = parser.parse_args()

    assert len(args)==1, 'please supply exactly 1 input argument\n%s'%__usage__
    config_path = args[0]

    #---------------------------------------------

    try:
        check_config(config_path, **vars(opts))

    except Exception as e: ### allow us to clean up forked jobs if needed (no zombies for us!
        print(traceback.format_exc().strip("\n")) 
        sys.exit(1)
