#!/usr/bin/env python

__usage__ = "idq-streaming_report [--options] config.ini [gps_start [gps_end]]"
__description__ = "an executable that summarizes idq output in a standard way. Can be used to manage things like GraceDb uploads or possibly queries from the visualization server"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#-------------------------------------------------

import os
import sys
import traceback
from optparse import OptionParser

### dependencies not in standard Python libraries
from idq import stream
from idq import configparser
from idq import logs
from idq import names
from idq import utils

#-------------------------------------------------

### parse arguments
parser = OptionParser(usage=__usage__, description=__description__)

parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print to stdout in addition to writing to automatically generated log')
parser.add_option('--reportdir', default=None, type=str,
    help='users can specify a directory directly to avoid writing output into the default directory (under config\'s rootdir). \
Useful if creating a report for a run in a directory you do not control')

opts, args = parser.parse_args()

assert len(args) >= 1 and len(args) <= 3, 'please supply between 1 and 3 input arguments\n%s'%__usage__
config_path = args[0]
args[1:] = [float(_) for _ in args[1:]]
gps_start, gps_end = utils.parse_variable_args(args[1:], 2)

#-------------------------------------------------

try:
    stream.report(config_path, gps_start=gps_start, gps_end=gps_end, **vars(opts))

except Exception as e: ### allow us to clean up forked jobs if needed (no zombies for us!)
    trcbck = traceback.format_exc().strip("\n")

    ### print to log for safe-keeping
    config = configparser.path2config(config_path)
    tag = config.tag
    logger = logs.configure_logger(
        stream.logger,
        names.tag2report_logname(tag), 
        log_level=config.report['log_level'],
        rootdir=opts.reportdir if opts.reportdir is not None else names.tag2logdir(tag, rootdir=os.path.abspath(config.rootdir)),
        verbose=opts.verbose,
    )
    logger.warn(trcbck)

    sys.exit(1)
