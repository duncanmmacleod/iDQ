
iDQ
==========================================================================

[![pipeline status](https://git.ligo.org/lscsoft/iDQ/badges/master/pipeline.svg)](https://git.ligo.org/lscsoft/iDQ/commits/master)
[![coverage](https://git.ligo.org/lscsoft/iDQ/badges/master/coverage.svg)](https://git.ligo.org/lscsoft/iDQ/commits/master)
[![conda-forge](https://img.shields.io/conda/vn/conda-forge/idq.svg)](https://anaconda.org/conda-forge/idq)

|              |        |
| ------------ | ------ |
| **Version:** | 0.4.0  |
| **Docs:**     | https://lscsoft.docs.ligo.org/iDQ  |
| **Source:**  | http://software.igwn.org/lscsoft/source/iDQ-0.4.0.tar.gz  |

iDQ is an engine for statistical inference. It specifically focuses on the problem of non-Gaussian
noise transients within gravitational-wave detectors, but the underlying formalism has broader applicability.

iDQ works with vectorized representations of the detector’s auxiliary state and searches for correlations
between that vectorized state and noise transients in h(t) with the end goal of producing a calibrated
estimate of the probability that there is a noise artifact in h(t), conditioned on the auxiliary state,
as a function of time. This is primarily done through supervised learning, and iDQ supports a variety of
supervised learning techniques. Furthermore, these concepts extend well beyond 1-dimensional data
(i.e.: timeseries) and could be applied to any streaming classification problem.

iDQ not only supports classification through 2-class classification schemes, but also supports automatic
retraining and calibration to deal with detector non-stationarity. In this way, the algorithm automatically
re-learns which correlations are important as they change over time and returns meaningful probabilistic
statements that can be interpreted immediately without further processing.

## Installation/Quickstart

  * [Installation](https://docs.ligo.org/lscsoft/iDQ/installation.html)
  * [Running Batch Jobs](https://docs.ligo.org/lscsoft/iDQ/tutorials/running_batch_pipeline.html)
  * [Running Streaming Jobs](https://docs.ligo.org/lscsoft/iDQ/tutorials/running_stream_pipeline.html)

## References

  * [iDQ: Statistical Inference of Non-Gaussian Noise with Auxiliary Degrees of Freedom in Gravitational-Wave Detectors](https://arxiv.org/abs/2005.12761)

-------------------------------------------------

## For Developers

### Python Style Guide

  - Objects should return themselves when there is no other obvious data product. For example, 
    - SupervisedClassifier.train should build an internal model and then return a copy of itself whereas 
    - SupervisedClassifier.timeseries should return a list of arrays of ranks, one element for each segment requested (contained within the classifier_data object passed)
  - spaces instead of tabs, with 4-spaces per indent block
  - if there are more than 3 input arguments for a function call, the call should be broken across multiple lines with appropriate indentation
  - all functions and method names should follow the snake case convention, e.g. foo_bar(), where underscores are used to separate names

### Data formats

As much as possible, data written to disk should follow the Common Data Format recommendations
    - https://wiki.ligo.org/DetChar/CommonDataFormat
However, within memory we will not require any particular data format. For example, we will allow ourselves to instantiate and pass around numpy structured arrays instead of hd5f dataset objects even though we will always represent the numpy arrays as datasets when writing them to disk.
