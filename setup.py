#!/usr/bin/env python
__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Reed Essick (reed.essick@ligo.org)"

#-------------------------------------------------

import os
import re

from setuptools import find_packages, setup
import glob

#-------------------------------------------------

def find_version(path):
    """Extract the `__version__` string from the given file
    modified from the DQR setup.py, originally written by Duncan Macleod
    """
    with open(path, 'r') as fp:
        version_file = fp.read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

#-------------------------------------------------

requires = [
    'dqsegdb2 >= 1.0',
    'h5py >= 2.2.1',
    'matplotlib >= 1.2.0',
    'ligo-segments >= 1.0.0',
    'numpy >= 1.7.1',
    'pluggy >= 0.11',
    'scipy >= 0.12.1',
    'toml >= 0.9.4',
]

setup(
    name = 'iDQ',
    version=find_version(os.path.join('idq', '__init__.py')),
    url = 'https://git.ligo.org/lscsoft/iDQ',
    author = __author__,
    author_email = 'reed.essick@ligo.org',
    description = __description__,
    license = 'MIT',
    scripts = [
        'bin/idq-batch',            ### batch pipeline scripts
        'bin/idq-train',
        'bin/idq-calibrate',
        'bin/idq-evaluate',
        'bin/idq-timeseries',
        'bin/idq-report',
        'bin/idq-condor_batch',     ### batch pipeline condor scripts
        'bin/idq-condor_train',
        'bin/idq-condor_evaluate',
        'bin/idq-condor_calibrate',
        'bin/idq-condor_timeseries',
        'bin/idq-stream',           ### streaming pipeline scripts
        'bin/idq-streaming_train',
        'bin/idq-streaming_evaluate',
        'bin/idq-streaming_calibrate',
        'bin/idq-streaming_timeseries',
        'bin/idq-streaming_report',
        'bin/idq-monitor',           ### misc helper scripts
        'bin/idq-target-times',
        'bin/idq-check_config',
        'bin/idq-simtimeseries',    ### simulation scripts
    ],
    packages = find_packages(),
    package_data = {
        'etc': ['*'],
    },
    install_requires = requires,
    zip_safe = False,
)
