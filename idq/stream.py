__description__ = "a module that houses helper functions for streaming executables"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#---------------------------------------------------------------------------------------------------

from collections import deque
import logging
import multiprocessing as mp
import os
import time as TIME

import numpy as np

import traceback

from ligo.segments import segment, segmentlist

from . import batch
from . import calibration
from . import classifiers
from . import condor
from . import configparser
from . import factories
from . import features
from . import io
from . import logs
from . import names
from . import utils
from . import exceptions


logger = logging.getLogger('idq')
stream_logger = logging.getLogger('idq-stream')

#-------------------------------------------------
### reference variables that may be useful throughout

DEFAULT_STREAM_WORKFLOW = 'fork'
DEFAULT_STREAM_LOG_LEVEL = logs.DEFAULT_LOG_LEVEL
DEFAULT_INITIAL_LOOKBACK = 3600 # one hour

DEFAULT_GPS_END = 2000000000 # set to be +infty but we need something that can be cast to an int

DEFAULT_MIN_NEW_SAMPLES = 1
DEFAULT_MAX_SAMPLES = calibration.DEFAULT_MAX_SAMPLES
DEFAULT_MAX_STRIDE = calibration.DEFAULT_MAX_STRIDE

DEFAULT_PLOT_STRIDE = 10
DEFAULT_MAX_METRIC_SPAN = 60        # one minute
DEFAULT_MAX_LATENCY_SPAN = 4 * 3600 # four hours

#---------------------------------------------------------------------------------------------------

def gps_range(gps_start=None, gps_end=None):
    """
    figure out the GPS range for a streaming analysis
    """
    if gps_start is None:
        gps_start = utils.current_gps_time()
    if gps_end is None:
        gps_end = DEFAULT_GPS_END
    assert gps_end > gps_start, "gps_end (%.3f) must be > gps_start (%.3f)!"%(gps_end, gps_start)
    return gps_start, gps_end

def stream(
        config_path,
        gps_start=None,
        gps_end=None,
        verbose=False,
        initial_lookback=DEFAULT_INITIAL_LOOKBACK,
        stream_workflow=DEFAULT_STREAM_WORKFLOW,
        stream_log_level=DEFAULT_STREAM_LOG_LEVEL,
        monitoring_cadence=utils.DEFAULT_MONITORING_CADENCE,
        skip_report=False,
        persist=False,
    ):
    '''
    launch and manage all streaming processes
    '''
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    #---------------------------------------------
    ### set up logging
    #---------------------------------------------
    tag = config.tag

    rootdir = config.rootdir
    logdir = names.tag2logdir(tag, rootdir=rootdir)
    traindir = names.tag2traindir(tag, rootdir=rootdir)
    calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)

    if verbose:
        print("writing log : %s"%logdir)

    logs.configure_logger(
        stream_logger,
        names.tag2stream_logname(tag),
        log_level=stream_log_level,
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    stream_logger.info( "using config : %s"%config_path )
    stream_logger.info("writing logs to : %s"%logdir)

    #---------------------------------------------
    ### check that we don't need to generate intial models, calibration_maps
    #---------------------------------------------

    # make all the reporters we care about for this
    reporter_factory = factories.ReporterFactory()
    ritems = config.train['reporting']
    trainreporter = reporter_factory(traindir, gps_start, gps_end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'train')))
    ritems = config.calibrate['reporting']
    calibratereporter = reporter_factory(calibratedir, gps_start, gps_end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'calibrate')))

    # check that something exists for each classifier
    nicknames = config.classifiers

    if np.any([(trainreporter.preferred(names.nickname2train_topic(nickname)) is None) for nickname in nicknames]):
        stream_logger.info('preferred model not available for at least one classifier. Launching batch training job over [%.3f, %.3f]'%(gps_start-initial_lookback, gps_start))
        try:
            batch.train(gps_start-initial_lookback, gps_start, config_path, verbose=verbose) ### FIXME: this may not block if workflow==condor
        except:
            stream_logger.error('initial training job failed!')
            raise

    if np.any([(calibratereporter.preferred(names.nickname2calibrate_topic(nickname)) is None) for nickname in nicknames]):
        stream_logger.info('preferred CalibrationMap not available for at least one classifier. Launching batch evaluate and calibrate jobs over [%.3f, %.3f]'%(gps_start-initial_lookback, gps_start))
        try:
            ### note, we grab the preferred model within batch.evaluate jobs
            batch.evaluate(gps_start-initial_lookback, gps_start, config_path, verbose=verbose, preferred=True) ### FIXME: this may not block if workflow==condor
        except:
            stream_logger.error('initial evaluate job failed!')
            raise
        try:
            ### note, we do not grab preferred dataset because we expect this to exactly match the previous evalute job
            batch.calibrate(gps_start-initial_lookback, gps_start, config_path, verbose=verbose, preferred=False) ### FIXME: this may not block if workflow==condor
        except:
            stream_logger.error('initial calibrate job failed!')
            raise

    #---------------------------------------------
    ### for processes based on workflow
    #---------------------------------------------

    if stream_workflow=="block":
        raise ValueError('stream cannot manage processes via stream_workflow=block because it must launch several processes simultaneously')

    #---

    elif stream_workflow=='fork':
        stream_logger.info('launching asynchronous tasks in parallel via multiprocessing')

        args = (config_path,) ### set up common arguments
        kwargs = {'gps_start':gps_start, 'gps_end':gps_end, 'verbose':verbose}

        workers = []
        try:
            jobs = [(evaluate, 'evaluate'), (calibrate, 'calibrate'), (timeseries, 'timeseries')]
            if not skip_report:
                jobs.append((report, 'report'))
            for job, name in jobs:
                logger.info('forking %s'%name)
                proc = mp.Process(target=job, name=name, args=args, kwargs=kwargs)
                proc.start()
                workers.append(proc)

            stream_logger.info('forking train')
            kwargs['initial_lookback'] = initial_lookback
            proc = mp.Process(target=train, name='train', args=args, kwargs=kwargs)
            proc.start()
            workers.append(proc)

            if persist: ### stick around and watch forked procs if requested
                num_proc = len(workers)
                manager = utils.CadenceManager(utils.current_gps_time(), stride=monitoring_cadence, delay=0) ### hard coding delay here in case DEFAULT_DELAY is changed...
                while num_proc:
                    stream_logger.info('monitoring %d processes'%num_proc)

                    for i in range(num_proc):
                        proc = workers.pop(0)
                        if proc.is_alive(): ### process is still running
                            workers.append(proc)

                        else:
                            proc.join() ### make sure we send SIGKILL, which this should do
                            stream_logger.info('%s terminated with exitcode=%d'%(proc.name, proc.exitcode))
                            if proc.exitcode:
                                raise RuntimeError('%s terminated with exitcode=%d'%(proc.name, proc.exitcode))

                    num_proc = len(workers)
                    manager.wait() ### wait so we don't spam the OS

        except Exception as e:
            while workers:
                proc = workers.pop()
                proc.terminate()
            raise e

    #---

    elif stream_workflow=='condor':
        stream_logger.info('launching asynchronous tasks in parallel via condor')

        if initial_lookback != 0:
            raise ValueError('cannot currently launch streaming training process with non-zero initial lookback through Condor')

        dag_path = condor.stream_dag(
            gps_start,
            gps_end,
            config_path,
            rootdir,
            skip_report=skip_report,
            **config.condor
        )
        stream_logger.info( 'dag written to '+dag_path )

        exitcode = condor.submit_dag(dag_path, block=True) ### blocks until this is done
        if exitcode:
            raise RuntimeError('non-zero returncode for dag '+dag_path )

        if persist: ### stick around and watch forked procs if requested
            raise NotImplementedError('we currently cannot monitor condor for completion of streaming jobs...')

    #---

    else:
        raise ValueError('stream_workflow=%s not understood'%stream_workflow)

    return None

#-------------------------------------------------

def _train_and_report(conn, classifier, reporter, *args):
    """
    a helper function for forked workflow via multiprocessing
    """
    model = classifier.train(*args)
    reporter.start, reporter.end = model.start, model.end ### update this to match the model's provenance
    path = reporter.report(names.nickname2train_topic(classifier.nickname), model, preferred=True) ### report the final result
    conn.send((path, reporter.start, reporter.end)) ### NOTE: almost identical to batch._train_and_report, but we return more things

def train(config_path, gps_start=None, gps_end=None, verbose=False, initial_lookback=DEFAULT_INITIAL_LOOKBACK):
    '''
    run the training process
    '''
    ### set up gps ranges
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()
    dataloader_factory = factories.DataLoaderFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(config.rootdir)
    traindir = names.tag2traindir(tag, rootdir) ### used to store results
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    target_channel = config.samples['target_channel']
    target_bounds = configparser.config2bounds(config.samples['target_bounds'])

    dirty_bounds = configparser.config2bounds(config.samples['dirty_bounds'])
    dirty_window = config.samples['dirty_window']
    random_rate = config.train['random_rate']

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------

    logs.configure_logger(
        logger,
        names.tag2train_logname(tag),
        log_level=config.train['log_level'],
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------

    ### the boundaries for these will be re-set within the main loop, hence, set to zero initially
    ritems = config.train["reporting"]
    trainreporter = reporter_factory(traindir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'train')))

    ### other reporters used to record intermediate data products
    picklereporter = reporter_factory(traindir, gps_start, gps_end, flavor="pickle")
    segmentreporter = reporter_factory(traindir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    klassifiers = [] ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.info( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actually instantiate the object and add it to the list
        klassifiers.append(classifier_factory(nickname, rootdir=traindir, **items))
    klassifiersDict = dict((classifier.nickname,classifier) for classifier in klassifiers)

    ### sort classifiers by how they train (their syntax)
    bulktrain = []
    inctrain = []
    for classifier in klassifiers:
        if isinstance(classifier, classifiers.IncrementalSupervisedClassifier):
            inctrain.append(classifier)
        else:
            bulktrain.append(classifier)

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab data
    items = config.features
    logger.info( 'dataloader -> '+' '.join('%s:%s'%(k, v) for k, v in items.items()))
    items = configparser.add_missing_kwargs(items, group=names.tag2group(tag, 'train'))
    time = items.pop('time') ### extract the name of the column we use as time

    sitems = config.train['stream']
    logger.info('stream_processor -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    ### initialize stream object
    stream = StreamProcessor(gps_start, gps_end, dataloader_factory, logger=logger, buffer_kwargs=items, **sitems)

    # configure segdb if required
    if config.train.get('ignore_segdb', False):
        logger.info( "ignoring segdb" )

    else:
        segdb_intersect = config.segments["intersect"].split() if "intersect" in config.segments else []
        segdb_exclude = config.segments["exclude"].split() if "exclude" in config.segments else []
        segdb_url = config.segments["segdb_url"]
        stream.configure_segdb(segdb_url, intersect=segdb_intersect, exclude=segdb_exclude, reporter=segmentreporter)

    # set up initial data query
    segs = stream._query_segdb(gps_start-initial_lookback, gps_start)
    dataloader = dataloader_factory(gps_start-initial_lookback, gps_start, segs=segs, **items)
    data = dataloader.query()

    ### get target times and draw from random times
    new_target_times = dataloader.target_times(time, target_channel, target_bounds, segs=segs)
    new_random_times, new_clean_segs = dataloader.random_times(time, target_channel, dirty_bounds, dirty_window, random_rate, segs=segs)
    accumulated_target_times = len(new_target_times)
    accumulated_random_times = len(new_random_times)
    accumulated_clean_segs = new_clean_segs

    ### make a new dataset and add to current dataset
    dataset = factories.DatasetFactory(data).labeled(new_target_times, new_random_times)
    datasetreporter = reporter_factory(traindir, 0, 0, flavor="dataset") ### we'll set the start, end times within the main loop

    ### set up nickanmes for reporting
    train_nicknames = dict((classifier.nickname, names.nickname2train_topic(classifier.nickname)) for classifier in klassifiers)
    for classifier in inctrain: ### load in the preferred models so incremental stuff picks up where it left off
        classifier.model = trainreporter.retrieve(train_nicknames[nickname], preferred=True)

    #-----------------------------------------
    ### actually run jobs
    #-----------------------------------------
    min_new_samples = config.train.get('min_new_samples', DEFAULT_MIN_NEW_SAMPLES)
    max_samples = config.train.get('max_samples', DEFAULT_MAX_SAMPLES)
    max_stride = config.train.get('max_stride', DEFAULT_MAX_STRIDE)

    workflow = config.train['workflow']
    if workflow=='fork':
        monitoring_cadence = config.train.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        fork_manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
        workers = [] ### declare this here to avoid a possible race condition when error reporting below
 
    ### start up streaming pipeline
    logger.info( 'starting streaming training' )
    while stream.timestamp < gps_end:

        logger.info('--- train stride: [%.3f, %.3f) ---'%stream.seg)

        ### poll for new data
        new_data, segs = stream.poll()

        timestamp = segs[0][0] if len(segs) else stream.timestamp
        lvtm = utils.livetime(utils.segments_intersection(segs, new_data.segs))
        logger.info( 'acquired %.3f sec of data at %d'%(lvtm, timestamp) )

        ### if there is new samples, create a new umbrella and add to big umbrella
        if lvtm:
            ### get target times and draw from random times
            new_target_times = new_data.target_times(time, target_channel, target_bounds, segs=segs)
            accumulated_target_times += len(new_target_times)
            logger.info( 'identified %d new target times for a total of %d accumulated target times'%(len(new_target_times), accumulated_target_times) )

            new_random_times, new_clean_segs = new_data.random_times(time, target_channel, dirty_bounds, dirty_window, random_rate, segs=segs)
            accumulated_random_times += len(new_random_times)
            accumulated_clean_segs |= new_clean_segs
            logger.info( 'identified %d new random times for a total of %d accumulated random times'%(len(new_random_times), accumulated_random_times) )

            ### make a new dataset and add to big dataset
            new_dataset = factories.DatasetFactory(new_data).labeled(new_target_times, new_random_times)
            dataset += new_dataset

            ### after generating new times and datasets, filter by segments before combining data
            ### this prevents repeated samples from being added in if/when we add padding
            new_data.filter(segs, time=time)
            data += new_data

            ### launch a new training job if there is enough data
            if accumulated_target_times+accumulated_random_times < min_new_samples:
                logger.info('%d accumulated times < %d, waiting to launch new training jobs'%(accumulated_target_times+accumulated_random_times, min_new_samples))

            else: ### launch a new training job if there is enough data
                logger.info('%d accumulated times >= %d, launching new training jobs'%(accumulated_target_times+accumulated_random_times, min_new_samples))

                ### create a new dataset from the training data, times, and labels
                dataset = features.Dataset.from_datachunks(
                    data,
                    times=dataset.times,
                    labels=dataset.labels
                )

                ### filter dataset by livetime and number of samples
                if data.livetime > max_stride:
                    logger.info('filtering dataset to reduce the total livetime to %.3f sec'%max_stride)
                if len(dataset.times) > max_samples:
                    logger.info('filtering dataset to reduce number of target times to <= %d'%max_samples)
                dataset.flush(max_stride=max_stride, max_samples=max_samples)

                logger.info( 'launching training job for [%.3f, %.3f) (new [%.3f, %.3f))'%(dataset.start, dataset.end, segs[0][0], segs[-1][1]) )

                ### set timestamp for reporter
                trainreporter.start = dataset.start
                trainreporter.end = dataset.end

                datasetreporter.start = dataset.start
                datasetreporter.end = dataset.end
                if inctrain:
                    path = datasetreporter.report('newdataset', new_dataset)
                    logger.info( 'new_dataset written to '+path )
                if bulktrain:
                    path = datasetreporter.report('dataset', dataset)
                    logger.info( 'dataset written to '+path )

                ### record clean segments
                segmentreporter.start = dataset.start
                segmentreporter.end = dataset.end
                path = segmentreporter.report('cleanSegments', accumulated_clean_segs)
                logger.info( 'clean segments written to '+path )

                if workflow == "block": ### train classifiers in series
                    logger.info('training classifiers sequentially')

                    if inctrain:
                        ntarget = accumulated_target_times
                        nrandom = accumulated_random_times
                        for classifier in inctrain:
                            nickname = classifier.nickname
                            logger.info('training %s with new_dataset (%d target_times, %d random_times)'%(nickname, ntarget, nrandom))
                            model = classifier.train(new_dataset)
                            trainreporter.start, trainreporter.end = model.start, model.end ### update reporter to match model provenance
                            logger.info('reporting model')
                            path = trainreporter.report(train_nicknames[nickname], model, preferred=True) ### report the resulting model
                            logger.info( 'model for %s written to %s'%(nickname, path) )

                    if bulktrain:
                        ntarget, nrandom = dataset.vectors2num()
                        for classifier in bulktrain:
                            nickname = classifier.nickname
                            logger.info( 'training %s with dataset (%d target_times, %d random_times)'%(nickname, ntarget, nrandom) )
                            model = classifier.train(dataset)
                            trainreporter.start, trainreporter.end = model.start, model.end ### update reporter to match model provenance
                            logger.info('reporting model')
                            path = trainreporter.report(train_nicknames[nickname], model, preferred=True) ### report the resulting model
                            logger.info( 'model for %s written to %s'%(nickname, path) )

                elif workflow == "fork": ### parallelize via multiprocessing
                    logger.info('training classifiers in parallel via multiprocessing')

                    try:
                        workers = []
                        ### set up all the jobs
                        if inctrain:
                            ntarget = accumulated_target_times
                            nrandom = accumulated_random_times
                            for classifier in inctrain:
                                nickname = classifier.nickname
                                logger.info('training %s with new_dataset (%d target_times, %d random_times)'%(nickname, ntarget, nrandom))
                                conn1, conn2 = mp.Pipe()
                                args = (conn1, classifier, trainreporter, new_dataset)
                                proc = mp.Process(target=_train_and_report, args=args, name=nickname)
                                proc.start()
                                conn1.close()
                                workers.append( (proc, conn2, args) )

                        if bulktrain:
                            ntarget, nrandom = dataset.vectors2num()
                            for classifier in bulktrain:
                                nickname = classifier.nickname
                                logger.info( 'training %s with dataset (%d target_times, %d random_times)'%(nickname, ntarget, nrandom) )
                                conn1, conn2 = mp.Pipe()
                                args = (conn1, classifier, trainreporter, dataset)
                                proc = mp.Process(target=_train_and_report, args=args, name=nickname)
                                proc.start()
                                conn1.close()
                                workers.append( (proc, conn2, args) )

                        ### monitor forked jobs, cleaning up as they finish
                        while workers:
                            proc, conn2, args = workers.pop()
                            if proc.is_alive(): ### still going
                                workers.append( (proc, conn2, args) ) ### add it back in

                            else: ### process is done
                                if proc.exitcode: ### something went wrong with this process!
                                    raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )
            
                                proc.terminate() ### make sure we clean up the process
                                path, trainreporter.start, trainreporter.end = conn2.recv() ### do this because models may be too big for mp.Connection
                                klassifiersDict[proc.name].model = trainreporter.retrieve(train_nicknames[proc.name])
                                conn2.close()
                                logger.info( 'model for %s written to %s'%(proc.name, path) )

                            fork_manager.wait()

                    except Exception as e:
                        while workers:
                            proc, conn2, _ = workers.pop()
                            proc.terminate()
                            conn2.close()
                        raise e

                elif workflow == "condor": ### parallelize via condor
                    raise NotImplementedError

                else:
                    raise ValueError('workflow=%s not understood!'%workflow)

                ### re-set target and random time counters
                accumulated_target_times = 0
                accumulated_random_times = 0

                ### recreate the dataset without any transformations applied
                dataset = features.Dataset.from_datachunks(
                    data,
                    times=dataset.times,
                    labels=dataset.labels
                )

        else:
            logger.info('no identified times, skipping training')

        ### repeat at processing cadence
        stream.flush(retain=0) ### FIXME: worry about padding

#-------------------------------------------------

def evaluate(config_path, gps_start=None, gps_end=None, verbose=False):
    '''
    the realtime evaluation routine
    '''
    ### set up gps ranges
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()
    dataloader_factory = factories.DataLoaderFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(config.rootdir)
    evaluatedir = names.tag2evaluatedir(tag, rootdir=rootdir)
    traindir = names.tag2traindir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, evaluatedir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    ### get info for building datasets
    target_channel = config.samples['target_channel']
    target_bounds = configparser.config2bounds(config.samples['target_bounds'])

    dirty_bounds = configparser.config2bounds(config.samples['dirty_bounds'])
    dirty_window = config.samples['dirty_window']
    random_rate = config.evaluate['random_rate']

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------

    logs.configure_logger(
        logger,
        names.tag2evaluate_logname(tag),
        log_level=config.evaluate['log_level'],
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------

    ### the boundaries for these will be re-set within the main loop, hence, set to zero initially
    ritems = config.evaluate["reporting"]
    evaluatereporter = reporter_factory(evaluatedir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'evaluate')))
    ritems = config.train["reporting"]
    trainreporter = reporter_factory(traindir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'evaluate')))

    ### other reporters used to record intermediate data products
    picklereporter = reporter_factory(traindir, gps_start, gps_end, flavor="pickle")
    segmentreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    klassifiers = [] ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.info( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actually instantiate the object and add it to the list
        klassifiers.append(classifier_factory(nickname, rootdir=evaluatedir, **items))

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab data
    items = config.features
    logger.info( 'dataloader -> '+' '.join('%s:%s'%(k, v) for k, v in items.items()))
    items = configparser.add_missing_kwargs(items, group=names.tag2group(tag, 'evaluate'))
    time = items.pop('time') ### extract the name of the column we use as time

    sitems = config.evaluate['stream']
    logger.info('stream_processor -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    ### initialize stream object
    stream = StreamProcessor(gps_start, gps_end, dataloader_factory, logger=logger, buffer_kwargs=items, **sitems)

    # configure segdb if required
    if config.evaluate.get('ignore_segdb', False):
        logger.info( "ignoring segdb" )

    else:
        segdb_intersect = config.segments["intersect"].split() if "intersect" in config.segments else []
        segdb_exclude = config.segments["exclude"].split() if "exclude" in config.segments else []
        segdb_url = config.segments["segdb_url"]
        stream.configure_segdb(segdb_url, intersect=segdb_intersect, exclude=segdb_exclude, reporter=segmentreporter)

    ### set up nickanmes for reporting
    train_nicknames = dict((classifier.nickname, names.nickname2train_topic(classifier.nickname)) for classifier in klassifiers)
    evaluate_nicknames = dict((classifier.nickname, names.nickname2evaluate_topic(classifier.nickname)) for classifier in klassifiers)

    #-----------------------------------------
    ### actually run jobs
    #-----------------------------------------
    max_samples = config.evaluate.get('max_samples', DEFAULT_MAX_SAMPLES)

    workflow = config.evaluate['workflow']
    if workflow=='fork':
        monitoring_cadence = config.evaluate.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        fork_manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
        workers = [] ### declare this here to avoid a possible race condition when error reporting below

    ### start up streaming pipeline
    logger.info('starting streaming evaluation')
    while stream.timestamp < gps_end:

        logger.info('--- evaluate stride: [%.3f, %.3f) ---'%stream.seg)

        ### retrieve trained models
        for classifier in klassifiers:
            nickname = classifier.nickname
            model = trainreporter.retrieve(train_nicknames[nickname], preferred=True) # get the next model available
                                                                                      # assumes we poll more frequently than models are produced
            ### update if there is a new model
            if model is not None:
                logger.info( 'updating model for '+nickname )
                classifier.model = model

        ### poll for new data
        new_data, segs = stream.poll()

        timestamp = segs[0][0] if len(segs) else stream.timestamp
        lvtm = utils.livetime(utils.segments_intersection(segs, new_data.segs))
        logger.info( 'acquired %.3f sec of data at %d'%(lvtm, timestamp) )

        ### if there is new samples, create a new umbrella
        if lvtm: ### NOTE: we only condition on whether there is something new to do
                 ### and don't care about min_new_samples, max_samples, max_stride
                 ### we just always evaluate everything, which could cause us to bog down at times...

            evaluate_start = segs[0][0]
            evaluate_end = segs[-1][1]

            ### get target times and draw from random times
            target_times = new_data.target_times(time, target_channel, target_bounds, segs=segs)
            logger.info( 'identified %d target times'%len(target_times) )
            if len(target_times) > max_samples:
                logger.info('retaining %d target times'%max_samples)
                target_times = target_times[-max_samples:]

            random_times, clean_segs = new_data.random_times(time, target_channel, dirty_bounds, dirty_window, random_rate, segs=segs)
            logger.info( 'identified %d random times'%len(random_times) )
            if len(random_times) > max_samples:
                logger.info('retaining %d random times'%max_samples)
                random_times = random_times[-max_samples:]

            ### evaluate classifiers if there are times in which to do evaluation
            if len(target_times) > 0 or len(random_times) > 0:

                ### build a dataset
                dataset = factories.DatasetFactory(new_data).labeled(target_times, random_times)

                ### update reporter timestamps
                evaluatereporter.start = evaluate_start
                evaluatereporter.end = evaluate_end
                logger.info( 'launching evaluation job for [%.3f, %.3f)'%(evaluate_start, evaluate_end) )

                ### record clean segments
                segmentreporter.start = evaluate_start
                segmentreporter.end = evaluate_end
                path = segmentreporter.report('cleanSegments', clean_segs, preferred=True)
                logger.info( 'clean segments written to '+path )

                if workflow == 'block': ### evaluate with each classifier sequentially
                    logger.info('evaluating classifiers sequentially')

                    for classifier in klassifiers:
                        nickname = classifier.nickname
                        logger.info( 'evaluating dataset for %s'%nickname )
                        dataset = classifier.evaluate(dataset)
                        evaluatereporter.start, evaluatereporter.end = dataset.start, dataset.end ### manage provenance
                        logger.info('reporting dataset')
                        path = evaluatereporter.report(evaluate_nicknames[nickname], dataset, preferred=True)
                        logger.info( 'dataset written to %s'%path )

                elif workflow == 'fork': ### parallelize via multiprocessing
                    logger.info( 'evaluating datasets in parallel via multiprocessing' )

                    try:
                        workers = []
                        ### set up all the jobs
                        for classifier in klassifiers:
                            nickname = classifier.nickname
                            logger.info( 'evalutating dataset for %s'%nickname )
                            conn1, conn2 = mp.Pipe()
                            args = (conn1, classifier, evaluatereporter, dataset)
                            proc = mp.Process(target=batch._evaluate_and_report, args=args, name=nickname)
                            proc.start()
                            conn1.close()
                            workers.append( (proc, conn2, args) )

                        ### monitor forked jobs, cleaning up as they finish
                        while workers:
                            proc, conn2, args = workers.pop()
                            if proc.is_alive(): ### still going
                                workers.append( (proc, conn2, args) ) ### add it back in

                            else: ### process is done
                                if proc.exitcode: ### something went wrong with this process!
                                    raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )

                                proc.terminate() ### make sure we clean up the process
                                logger.info( 'evaluated dataset for %s written to %s'%(proc.name, conn2.recv()) )
                                conn2.close()

                            fork_manager.wait()

                    except Exception as e:
                        while workers:
                            proc, conn2, _ = workers.pop(0)
                            proc.terminate()
                            conn2.close()
                        raise e

                elif workflow == 'condor': ### parallelize via condor
                    raise NotImplementedError

                else:
                    raise ValueError('workflow=%s not understood!'%workflow)

            else:
                logger.info('no identified times, skipping evaluation')

        ### repeat at processing cadence
        stream.flush(retain=0) ### FIXME: worry about padding

#-------------------------------------------------

def _calibrate_and_report(conn, classifier, reporter, *args):
    '''
    a helper function for forked workflow via multiprocessing
    '''
    calibrate_kwargs = args[-1]
    args = args[:-1]
    calibmap = classifier.calibrate(*args, **calibrate_kwargs)
    reporter.start, reporter.end = calibmap.start, calibmap.end ### manage provenance
    path = reporter.report(names.nickname2calibrate_topic(classifier.nickname), calibmap, preferred=True) ### report the final result
    conn.send((path, reporter.start, reporter.end)) ### NOTE: almost identical to batch._calibrate_and_report but we return more stuff

def calibrate(config_path, gps_start=None, gps_end=None, verbose=False):
    '''
    calibrate the classifier predictions based on historical data
    '''
    ### set up gps ranges
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()
    dataloader_factory = factories.DataLoaderFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(config.rootdir)
    evaluatedir = names.tag2evaluatedir(tag, rootdir=rootdir)
    calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, evaluatedir, calibratedir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2calibrate_logname(tag),
        log_level=config.calibrate['log_level'],
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------

    ### the boundaries for these will be re-set within the main loop, hence, set to zero initially
    ritems = config.evaluate['reporting']
    evaluatereporter = reporter_factory(evaluatedir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'calibrate')))
    ritems = config.calibrate['reporting']
    calibratereporter = reporter_factory(calibratedir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'calibrate')))

    ### other reporters used to record intermediate data products
    picklereporter = reporter_factory(calibratedir, gps_start, gps_end, flavor="segment")
    cleansegsreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    klassifiers = [] ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.info( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actually instantiate the object and add it to the list
        klassifiers.append(classifier_factory(nickname, rootdir=calibratedir, **items))
    klassifiersDict = dict((classifier.nickname,classifier) for classifier in klassifiers)

    #-----------------------------------------
    # extract basic parameters for calibration
    #-----------------------------------------
    calibrate_kwargs = config.calibrate
    min_new_samples = calibrate_kwargs.pop('min_new_samples', DEFAULT_MIN_NEW_SAMPLES)

    # set up cadence manager
    sitems = config.evaluate['stream'] ### NOTE: THIS IS NOT A TYPO. We need the calibration and evaluation jobs to be tightly linked
                                       ### I'm very intentionally taking the stream cadence from the evaluation jobs for the calibration jobs
    logger.info('cadence_manager -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    assert 'max_timestamp' not in sitems, 'max_timestamp is set by hand to gps_end and cannot be passed through INI file via evaluate stream'
    manager = utils.CadenceManager(gps_start, max_timestamp=gps_end, logger=logger, **sitems)

    # define nicknames for persistent loop
    evaluate_nicknames = dict((classifier.nickname, names.nickname2evaluate_topic(classifier.nickname)) for classifier in klassifiers)
    calibrate_nicknames = dict((classifier.nickname, names.nickname2calibrate_topic(classifier.nickname)) for classifier in klassifiers)
    for classifier in klassifiers: ### load in existing calibration maps so jobs pick up where they left off
        classifier.calibration_map = calibratereporter.retrieve(calibrate_nicknames[classifier.nickname], preferred=True)

    # set up accumulated datasets as needed
    # NOTE: we only need to do this here because everything else uses StreamProcessor to avoid reading in redundant samples, etc
    datasets = {}
    for classifier in klassifiers:
        dataset = features.Dataset(start=manager.timestamp, end=manager.timestamp)
        datasets[classifier.nickname] = dataset

    ### set up accumulated clean segments for calibration
    clean_segs = segmentlist([])
    
    #-----------------------------------------
    ### actually run jobs
    #-----------------------------------------
    workflow = config.calibrate['workflow']
    if workflow=='fork':
        monitoring_cadence = config.calibrate.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        fork_manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
        workers = [] ### declare this here to avoid a possible race condition when error reporting below

    ### start up streaming pipeline
    logger.info('starting streaming calibration')
    while manager.timestamp < gps_end:

        logger.info('--- calibrate stride: [%.3f, %.3f) ---'%manager.seg)

        timestamp, _ = manager.wait() ### manage cadence and sleep logic here

        logger.info('retrieving clean segments')
        new_clean_segs = cleansegsreporter.retrieve('cleanSegments', preferred=True)
        if new_clean_segs is None: ### nothing new available
            continue
        clean_segs = utils.segments_union(clean_segs, new_clean_segs)

        ### read in new data for all classifiers
        for classifier in klassifiers:
            nickname = classifier.nickname
            dataset = datasets[nickname]

            new_dataset = evaluatereporter.retrieve(evaluate_nicknames[nickname], preferred=True) # get the next dataset available
            if new_dataset is None: ### nothing new available
                continue

            elif (len(new_dataset)==0) or (new_dataset.end <= dataset.end):
                logger.info('no new data for %s at %d'%(nickname, timestamp))

            elif new_dataset.start >= dataset.end: ### represents new data!
                ngch, ncln = new_dataset.vectors2num()
                logger.info('acquired %.3f sec of data for %s at %d (%d glitch, %d clean)'%\
                    (utils.livetime(new_dataset.segs), nickname, timestamp, ngch, ncln)
                )
                dataset += new_dataset

            elif new_dataset.end > dataset.end: ### at least some of the new_dataset is actually new
                trunc = utils.segments_intersection(new_dataset.segs, segmentlist([(dataset.end, new_dataset.end)]))
                ngch, ncln = new_dataset.vectors2num()
                logger.info('acquired %.3f sec of data for %s at %d (%d glitch, %d clean)'%\
                    (utils.livetime(trunc), nickname, timestamp, ngch, ncln)
                )
                new_dataset.filter(segs=trunc)
                dataset += new_dataset

            datasets[nickname] = dataset

        ### launch jobs
        if workflow == 'block':
            logger.info( 'calibrating classifiers with evaluated datasets sequentially' )

            for classifier in klassifiers:
                nickname = classifier.nickname
                dataset = datasets[nickname]

                if len(dataset) >= min_new_samples: ### need to spawn a calibration job for this classifier
                    calibratereporter.start = dataset.start
                    calibratereporter.end = dataset.end

                    ### limit clean segments to those used by calibration
                    calib_clean_segs = utils.segments_intersection(clean_segs, dataset.segs)

                    ### evaluate dataset and save probabilities to disk
                    ngch, ncln = dataset.vectors2num()
                    logger.info( 'calibrating %s (%d glitch, %d clean)'%(nickname, ngch, ncln) )
                    calibmap = classifier.calibrate(dataset, clean_segs=calib_clean_segs, **calibrate_kwargs) ### this works incrementally be default

                    calibratereporter.start, calibratereporter.end = calibmap.start, calibmap.end ### manage provenance
                    logger.info('reporting calibration')
                    path = calibratereporter.report(calibrate_nicknames[nickname], calibmap, preferred=True)
                    logger.info( 'calibration written to %s'%path )

                    ### re-set dataset
                    start = dataset.end ### take as my starting point where the old dataset left off
                    datasets[nickname] = features.Dataset(start=start, end=start)

                else:
                    logger.info('not enough new data to re-calibrate %s (%d samples; required at least %d)'%(nickname, len(dataset), min_new_samples))

        elif workflow == 'fork':
            logger.info( 'calibrating in parallel via multiprocessing' )

            try:
                workers = []
                ### set up all the jobs
                for classifier in klassifiers:
                    nickname = classifier.nickname
                    dataset = datasets[nickname]

                    if len(dataset) >= min_new_samples: ### need to spawn a calibration job for this classifier
                        calibratereporter.start = dataset.start
                        calibratereporter.end = dataset.end

                        ### limit clean segments to those used by calibration
                        calib_clean_segs = utils.segments_intersection(clean_segs, dataset.segs)

                        ### evaluate dataset and save probabilities to disk
                        ngch, ncln = dataset.vectors2num()
                        logger.info( 'calibrating %s (%d glitch, %d clean)'%(nickname, ngch, ncln) )
                        all_kwargs = {'clean_segs': calib_clean_segs}
                        all_kwargs.update(calibrate_kwargs)

                        conn1, conn2 = mp.Pipe()
                        args = (conn1, classifier, calibratereporter, dataset, all_kwargs)
                        proc = mp.Process(target=_calibrate_and_report, args=args, name=nickname)
                        proc.start()
                        conn1.close()
                        workers.append( (proc, conn2, args) )

                    else:
                        logger.info('not enough new data to re-calibrate %s (%d samples; required at least %d)'%(nickname, len(dataset), min_new_samples))

                ### monitor forked jobs, cleaning up as they finish
                while workers:
                    proc, conn2, args = workers.pop()
                    if proc.is_alive(): ### still going
                        workers.append( (proc, conn2, args) ) ### add it back in

                    else: ### process is done
                        if proc.exitcode: ### something went wrong with this process!
                            raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )

                        proc.terminate() ### make sure we clean up the process

                        path, calibratereporter.start, calibratereporter.end = conn2.recv() # do this because CalibMaps may be too big for mp.Connection
                        conn2.close()
                        klassifiersDict[proc.name].calibration_map = calibratereporter.retrieve(calibrate_nicknames[proc.name])
                        logger.info( 'calibration for %s written to %s'%(proc.name, path) )

                        ### re-set dataset
                        dataset = datasets[proc.name]
                        start = dataset.end ### take as my starting point where the old dataset left off
                        datasets[proc.name] = features.Dataset(start=start, end=start)

                    fork_manager.wait()

            except Exception as e:
                while workers:
                    proc, conn2, _ = workers.pop(0)
                    proc.terminate()
                    conn2.close()
                raise e

        elif workflow == 'condor':
            raise NotImplementedError

        else:
            raise ValueError('workflow=%s not understood!'%workflow)

#-------------------------------------------------

def timeseries(config_path, gps_start=None, gps_end=None, verbose=False):
    '''
    the realtime evaluation routine
    '''
    ### set up gps ranges
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()
    dataloader_factory = factories.DataLoaderFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(config.rootdir)
    timeseriesdir = names.tag2timeseriesdir(tag, rootdir=rootdir)
    calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)
    traindir = names.tag2traindir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, calibratedir, timeseriesdir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    ### extract information necessary for channel names in timeseries files
    instrument = config.instrument

    target_channel = config.samples['target_channel']
    target_bounds = configparser.config2bounds(config.samples['target_bounds'])
    frequency = config.features['frequency']
    assert frequency in target_bounds, 'must specify a frequency range (called %s) within target_bounds'%frequency
    freq_min, freq_max = target_bounds[frequency]
    assert isinstance(freq_min, int) and isinstance(freq_max, int), 'frequency bounds must be integers!'

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2timeseries_logname(tag),
        log_level=config.timeseries['log_level'],
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------

    ### the boundaries for these will be re-set within the main loop, hence, set to zero initially
    ritems = config.train['reporting']
    trainreporter = reporter_factory(traindir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'timeseries')))
    ritems = config.calibrate['reporting']
    calibratereporter = reporter_factory(calibratedir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'timeseries')))
    ritems = config.timeseries['reporting']
    timeseriesreporter = reporter_factory(timeseriesdir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'timeseries')))

    ### other reporters used to record intermediate data products
    picklereporter = reporter_factory(calibratedir, gps_start, gps_end, flavor="pickle")
    segmentreporter = reporter_factory(timeseriesdir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    klassifiers = [] ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.info( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actually instantiate the object and add it to the list
        klassifiers.append(classifier_factory(nickname, rootdir=timeseriesdir, **items))

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab data
    items = config.features
    logger.info( 'dataloader -> '+' '.join('%s:%s'%(k, v) for k, v in items.items()))
    items = configparser.add_missing_kwargs(items, group=names.tag2group(tag, 'timeseries'))

    ### figure out timeseres sampling rate -> dt
    srate = config.timeseries['srate']
    logger.info('generating timeseries sampled at %.3f Hz'%srate)
    dt = 1./srate ### we pass dt to classifier.timeseries so we only have to do division once.

    ### initialize stream processor
    sitems = config.timeseries['stream']
    logger.info('stream_processor -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    stream = StreamProcessor(gps_start, gps_end, dataloader_factory, logger=logger, buffer_kwargs=items, **sitems)

    # configure segdb if required
    if config.evaluate.get('ignore_segdb', False):
        logger.info( "ignoring segdb" )

    else:
        segdb_intersect = config.segments["intersect"].split() if "intersect" in config.segments else []
        segdb_exclude = config.segments["exclude"].split() if "exclude" in config.segments else []
        segdb_url = config.segments["segdb_url"]
        stream.configure_segdb(segdb_url, intersect=segdb_intersect, exclude=segdb_exclude, reporter=segmentreporter)

    train_nicknames = dict((classifier.nickname, names.nickname2train_topic(classifier.nickname)) for classifier in klassifiers)
    calibrate_nicknames = dict((classifier.nickname, names.nickname2calibrate_topic(classifier.nickname)) for classifier in klassifiers)
    timeseries_nicknames = dict((classifier.nickname, names.nickname2timeseries_topic(instrument, classifier.nickname)) for classifier in klassifiers)
    templates = dict((classifier.nickname, names.channel_name_template(instrument, classifier.nickname, freq_min, freq_max)) for classifier in klassifiers)

    #-----------------------------------------
    ### actually run jobs
    #-----------------------------------------
    workflow = config.timeseries['workflow']
    if workflow=='fork':
        monitoring_cadence = config.timeseries.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        fork_manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
        workers = [] ### declare this here to avoid a possible race condition when error reporting below

    ### start up streaming pipeline
    logger.info('starting streaming timeseries')
    while stream.timestamp < gps_end:

        logger.info('--- timeseries stride: [%.3f, %.3f) ---'%stream.seg)

        # update internals for each classifier
        for classifier in klassifiers:
            nickname = classifier.nickname

            ### retrieve trained model
            model = trainreporter.retrieve(train_nicknames[nickname], preferred=True) # get the next model available
                                                                                      # assumes we poll faster than models are produced
            ### update if there is a new model
            if model is not None:
                logger.info( 'updating model for '+nickname )
                classifier.model = model

            ### retrieve calibration map
            calibration_map = calibratereporter.retrieve(calibrate_nicknames[nickname], preferred=True) # get the next map available
                                                                                                        # assumes we poll faster than maps are produced
            ### update if there is a new calibration map
            if calibration_map:
                logger.info( 'updating calibration map for '+nickname )
                classifier.calibration_map = calibration_map


        ### poll for new data
        ### NOTE: we very intentionally to not "fix_segments" 
        ### keeping them partitioned this way tells us which frames to write 
        ### so that each frame has the same (predictable) duration: stream.stride
        new_data, segs = stream.poll()

        timestamp = segs[0][0] if len(segs) else stream.timestamp
        lvtm = utils.livetime(utils.segments_intersection(segs, new_data.segs))
        logger.info( 'acquired %.3f sec of data at %d'%(lvtm, timestamp) )

        ### create timeseries if there is new data
        if lvtm:
            dataset_factory = factories.DatasetFactory(new_data)

            for seg in segs: ### process a single segment at a time to guarantee uniform duration (stream.stride)
                seglist = segmentlist([seg])

                ### update report times
                timeseriesreporter.start, timeseriesreporter.end = seg

                if workflow == 'block':
                    logger.info('processing classifiers sequentially')
                    for classifier in klassifiers:
                        nickname = classifier.nickname

                        ### generate series
                        logger.info('generating timeseries for %s'%nickname )
                        series = classifier.timeseries(dataset_factory, dt=dt, segs=seglist)

                        ### calibrate
                        logger.info('calibrating timeseries')
                        seriesdict = calibration.series2dict(series, classifier.model, classifier.calibration_map, templates[nickname])

                        ### report probabilities
                        logger.info('reporting probabilities')
                        for path in batch.report_timeseries(timeseriesreporter, timeseries_nicknames[nickname], seriesdict, preferred=True):
                            logger.info( 'probabilities written to %s'%path )

                        ### record latency
                        time = float(seg[0])
                        latency = utils.gps2latency(time)
                        logger.info( 'latency upon writing probabilities: %.3f'%latency )

                elif workflow == 'fork': ### parallelize via multiprocessing
                    logger.info( 'generating timeseries in parallel via multiprocessing' )

                    try:
                        ### set up all the jobs
                        for classifier in klassifiers:
                            nickname = classifier.nickname
                            logger.info( 'generating timeseries for %s'%nickname )
                            conn1, conn2 = mp.Pipe()
                            args = (conn1, classifier, timeseriesreporter, instrument, (dataset_factory, dt, segs), names.channel_name_template(instrument, nickname, freq_min, freq_max))
                            proc = mp.Process(target=batch._timeseries_and_report, args=args, name=nickname)
                            proc.start()
                            conn1.close()
                            workers.append( (proc, conn2, args) )

                        ### monitor forked jobs, cleaning up as they finish
                        while workers:
                            proc, conn2, args = workers.pop()
                            if proc.is_alive(): ### still going
                                workers.append( (proc, conn2, args) ) ### add it back in

                            else: ### process is done
                                if proc.exitcode: ### something went wrong with this process!
                                    raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )

                                proc.terminate() ### make sure we clean up the process
                                for path in conn2.recv():
                                    logger.info( 'timeseries for %s written to %s'%(proc.name, path) )
                                conn2.close()

                                ### record latency
                                time = float(seg[0])
                                latency = utils.gps2latency(time)
                                logger.info( 'latency upon writing probabilities: %.3f'%latency )

                            fork_manager.wait()

                    except Exception as e:
                        while workers:
                            proc, conn2, _ = workers.pop(0)
                            proc.terminate()
                            conn2.close()
                        raise e

                elif workflow == 'condor': ### parallelize via condor
                    raise NotImplementedError

                else:
                    raise ValueError('workflow=%s not understood!'%workflow)

        else:
            logger.info('no identified times, skipping timeseries')

        ### repeat at processing cadence
        stream.flush(retain=0) ### FIXME: worry about padding

#-------------------------------------------------

def report(config_path, gps_start=None, gps_end=None, reportdir=None, verbose=False):
    """run batch.report jobs periodically
    """
    ### set up gps ranges
    gps_start, gps_end = gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    tag = config.tag
    rootdir = os.path.abspath(config.rootdir)

    if reportdir is None:
        logdir = names.tag2logdir(tag, rootdir=rootdir)
    else:
        logdir = reportdir ### write log into the output directory

    logs.configure_logger(
        logger,
        names.tag2report_logname(tag),
        log_level=config.report['log_level'],
        rootdir=logdir,
        verbose=verbose,
        rotating=logs.DEFAULT_ROTATION_CADENCE,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #---------------------------------------------
    ### set up cadence manager
    #---------------------------------------------
    # support a running average if specified
    lookback = config.report.get('lookback', 0)

    sitems = config.report['stream'] ### NOTE: THIS IS NOT A TYPO. We need the calibration and evaluation jobs to be tightly linked
                                             ### I'm very intentionally taking the stream cadence from the evaluation jobs for the calibration jobs
    logger.info('cadence_manager -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    assert 'max_timestamp' not in sitems, 'max_timestamp is set by hand to gps_end and cannot be passed through INI file via evaluate stream'
    manager = utils.CadenceManager(gps_start, max_timestamp=gps_end, logger=logger, **sitems)

    #---------------------------------------------
    ### actually loop
    #---------------------------------------------

    logger.info('starting streaming reporting')
    while manager.timestamp < gps_end:

        logger.info('--- report stride: [%.3f, %.3f) ---'%manager.seg)
        start, end = manager.wait() ### manage cadence and sleep logic here

        try:
            ### we allow users to generate a running-average with non-trivial overlap via "lookback"
            batch.report(start-lookback, end, config_path, reportdir=reportdir, verbose=verbose)

        except KeyboardInterrupt:
            raise ### we want to raise this one

        except: # but catch anything else
            logger.warn('report job failed!')
            logger.warn(traceback.format_exc().strip("\n"))

#-------------------------------------------------

class StreamProcessor(object):
    """
    handles data processing logic for the streaming pipeline
    """
    # define a few string templates for things that will be printed repeated
    _poll_template = 'querying for triggers within [%.4f, %.4f)'
    _retained_template = 'retained %.4f sec of livetime'
    _ignore_template = 'ignoring segdb'
    _yesdata_template = 'found data at %.4f'
    _nodata_template = 'no data found within [%.4f, %.4f)'
    _incontiguous_template = 'gap in data found [%.4f, %.4f). Moving to %.4f'
    _latency_template = 'too far behind realtime at (%.4f sec at %.4f), skipping ahead to %.4f'
    _newbuffer_template = 'creating new buffer for [%.4f, %.4f)'

    def __init__(self, start, end, buffer_factory, buffer_kwargs={}, logger=None, **manager_kwargs):

        ### record things
        self._logger = logger # hold on to this reference so we can report about what happens internally here...
                              # this is kinda janky, but it will work...
        self._init_segdb()
        self._buffer_factory = buffer_factory

        ### set up cadence manager
        assert (not 'max_timestamp' in manager_kwargs), 'max_timestamp is set to end by hand, so it cannot be passed via manager_kwargs!'
        self._manager = utils.CadenceManager(start, max_timestamp=end, logger=logger, **manager_kwargs)

        ### do this next little backflip to make sure all options are passed correctly
        self._buffer_kwargs = buffer_kwargs
        self._buffer_kwargs.update(self._buffer_factory(start, end, **buffer_kwargs).kwargs)
        self._buffer = None

        ### set up the buffer store
        self._queue = deque()

    def _init_segdb(self):
        """
        set up all the attributes needed for segdb stuff
        these will all be overwritten upon a call to configure_segdb
        """
        self._ignore_segdb = True ### default behavior
        self._segdb_url = None
        self._segdb_intersect = None
        self._segdb_exclude = None
        self._segdb_reporter = None

    @property
    def ignore_segdb(self):
        return self._ignore_segdb

    @property
    def stride(self):
        return self._manager.stride

    @property
    def delay(self):
        return self._manager.delay

    @property
    def timestamp(self):
        return self._manager.timestamp

    @property
    def seg(self):
        return self._manager.seg

    @property
    def segs(self):
        return self._manager.segs

    @property
    def segdb_url(self):
        return self._segdb_url

    @property
    def segdb_intersect(self):
        return self._segdb_intersect

    @property
    def segdb_exclude(self):
        return self._segdb_exclude

    @property
    def segdb_reporter(self):
        return self._segdb_reporter

    def configure_segdb(self, segdb_url, intersect=[], exclude=[], reporter=None):
        """
        we have to configure_segdb in order to not ignore_segdb, and we assume that one would only configure_segdb if you wanted to use it
        thus, we manage the boolean flag controlling whether to use segdb based on whether this method was called
        """
        self._ignore_segdb = False
        self._segdb_url = segdb_url
        self._segdb_intersect = intersect
        self._segdb_exclude = exclude
        self._segdb_reporter = reporter

        self._segdb_template = "querying %s within %s for: intersect=%s ; exclude=%s"%(segdb_url, '[%.3f, %.3f)', ','.join(intersect), ','.join(exclude))

        ### update the existing buffer
        if self._buffer is not None:
            self._buffer.filter(self._query_segdb(self._buffer.start, self._buffer._end))

    def _query_segdb(self, start, end):
        """
        a helper function that manages segment construction for each buffer
        """
        if self.ignore_segdb:
            if self._logger is not None:
                self._logger.info(self._ignore_template)
            segs = segmentlist([segment(start, end)])

        else:
            if self._logger is not None:
                self._logger.info(self._segdb_template%(start, end))

            segs = utils.segdb2segs(
              start,
              end,
              intersect=self._segdb_intersect,
              exclude=self._segdb_exclude,
              segdb_url=self._segdb_url,
              logger=self._logger,
           )

            if self._logger is not None:
                self._logger.info(self._retained_template%utils.livetime(segs))

            if self._segdb_reporter is not None:
                self._segdb_reporter.start = start
                self._segdb_reporter.end = end
                path = self._segdb_reporter.report('segments', segs)
                self._logger.info( 'segments written to '+path )

        return segs

    def new_buffer(self, start):
        end = start + self.stride
        if self._logger is not None:
            self._logger.info(self._newbuffer_template%(start, end))
        return self._buffer_factory(start, end, segs=self._query_segdb(start, end), **self._buffer_kwargs)

    def flush(self, retain=0):
        ### clear out all but the most recently added buffer, which is done
        ### so new samples can access older data to create feature vectors
        while len(self._queue) > retain:
            self._queue.popleft()

    def wait(self):
        """
        delegates to CadenceManager.wait(), which updates timestamp after sleeping
        """
        return self._manager.wait()

    def poll(self, **triggers_kwargs):
        """
        the workhorse method
        manages requests for data and I/O via ClassifierData objects
        constructs and returns an umbrella of the requested ClassifierDatas for this stride

        triggers_kwargs is passed to calls to ClassifierData.triggers and is exposed to help control I/O costs
        """
        log = self._logger is not None
        segs = segmentlist([])
        try:
            for seg in self._manager.poll(): ### manager.poll will gobble up multiple segments if needed
                self._buffer = self.new_buffer(seg[0]) ### we wait to create this until after self.wait() because queries to segdb 
                                                  ### within new_buffer() must be causal
                if utils.livetime(self._buffer.segs): ### there is non-trivial livetime in the new buffer
                    if log:
                        self._logger.info(self._poll_template%(self._buffer.start, self._buffer.end))

                    try: ### try to retrieve data
                        data = self._buffer.query(**triggers_kwargs)

                    except exceptions.IncontiguousDataError as e: ### span of data doesn't match span of classifier data
                       if log:
                            self._logger.warn(self._incontiguous_template%(e.gap_start, e.gap_end, e.timestamp))
                       self._manager.timestamp = e.timestamp

                    except exceptions.NoDataError as e: ### no data found when querying
                        if log:
                            self._logger.warn(self._nodata_template%(e.timestamp, e.timestamp+e.stride))

                    else:
                        if log:
                            self._logger.info(self._yesdata_template%seg[0])
                        self._queue.append(data)

                segs.append(seg)

        except exceptions.LatencyError as e:
            if self._logger is not None:
                self._logger.warn(self._latency_template%(e.latency, e.current_timestamp, e.timestamp))
            self._manager.timestamp = e.timestamp

        ### return children of umbrella corresponding to new times
        return features.combine_chunks(list(self._queue)), segs ### FIXME: worry about padding and edge effects for feature vectors?
