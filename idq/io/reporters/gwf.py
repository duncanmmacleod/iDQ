__description__ = "a python module housing gwf-based reporters"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os

import numpy as np

from lal import CreateREAL8TimeSeries
from lal import CreateREAL8Sequence
from lal import DimensionlessUnit
from lal import LALDETECTORTYPE_ABSENT

from LDAStools import frameCPP as frcpp

from ... import hookimpl
from ... import utils
from . import DiskReporter


#-------------------------------------------------
### reporter implementations

class GWFSeriesReporter(DiskReporter):
    """
    store series informaiton in gwf files

    NOTE:

      * GWFSeriesReporter currently only supports a single contiguous segment and will raise an exception if it finds more than one.
      * This is most likely the most common use case (e.g.: streaming timeseries ignoring segdb), so it's probably fine
    """
    _suffix = 'gwf'

    _MODEL_NAME = 'IDQ_MODEL'
    _CALIB_NAME = 'IDQ_CALIB'

    def _write(self, path, seriesdict, run=0, **kwargs):
        if isinstance(seriesdict, list):
            assert len(seriesdict)==1, 'GWFSeriesReporter currently only supports a single contiguous segment of data'
            seriesdict = seriesdict[0]

        t0 = seriesdict['t0']
        deltaT = seriesdict['deltaT']
        N = len(list(seriesdict['data'].values())[0]) ### NOTE: this could be fragile...

        ### make the frame
        frame = frcpp.FrameH()
        frame.SetName('-'.join(os.path.basename(path).split('-')[:-2])) ### NOTE: could be fragile
        frame.SetGTime(frcpp.GPSTime(*[int(_) for _ in utils.float2sec_ns(t0)]))
        frame.SetDt(N*deltaT)
        frame.SetRun(run)

        ### add provenance information
        frame.AppendFrHistory(frcpp.FrHistory(self._MODEL_NAME, utils.current_gps_time(), str(seriesdict['model'])))
        frame.AppendFrHistory(frcpp.FrHistory(self._CALIB_NAME, utils.current_gps_time(), str(seriesdict['calib'])))

        ### add data
        for key, data in seriesdict['data'].items():
            # make a vector
            frvect = frcpp.FrVect(
                key,
                frcpp.FrVect.FR_VECT_8R,
                1, ### number of dimensions
                frcpp.Dimension( # the dimension object
                    N, ### number of elements
                    deltaT, ### spacing
                    's', ### units
                    0, ### start time offset relative to the header (set above)
                ),
                '', ### dimensionless unit, cause we have probabilities and such
            )
            frvect.GetDataArray()[:] = data ### actually populate the object

            # add that vector to the ProcData
            frdata = frcpp.FrProcData(
                key,
                '', ### no comment
                frcpp.FrProcData.TIME_SERIES,  # ID as time-series
                frcpp.FrProcData.UNKNOWN_SUB_TYPE,  # empty sub-type (fseries)
                0,  # offset of first sample relative to frame start
                N*deltaT,  # duration of data
                0.,  # heterodyne frequency
                0.,  # phase of heterodyne
                0.,  # frequency range
                0.,  # resolution bandwidth
            )
            frdata.AppendData(frvect) ### add the vector to this data

            # add ProcData to FrameH
            frame.AppendFrProcData(frdata)

        ### write
        frame.Write(frcpp.OFrameFStream(path))

    def _read(self, path):
        seriesdict = dict()
        stream = frcpp.IFrameFStream(path)
        frame = stream.ReadNextFrame() ### assume a single frame

        ### extract data
        toc = stream.GetTOC()
        seriesdict = dict()
        for channels, foo in [(toc.GetProc(), stream.ReadFrProcData), (toc.GetADC(), stream.ReadFrAdcData), (toc.GetSim(), stream.ReadFrSimData)]:
            for channel in channels:
                data = foo(0, channel) ### magic number: one frame per stream...
                dim = data.data[0].GetDim(0)
                data = data.data[0].GetDataArray() ### magic number: one FrVect wrapper per channel
                seriesdict[channel] = np.empty_like(data, dtype=float)
                seriesdict[channel][:] = data[:]

        # assume there is at least one channel
        t0 = utils.sec_ns2float(*frame.GetGTime())
        ### NOTE: these should be the same for all channels, so we only read them once
        deltaT = dim.dx

        ### extract histories
        histories = dict()
        for history in frame.RefHistory():
            histories[history.GetName()] = history.GetComment()

        for name in [self._MODEL_NAME, self._CALIB_NAME]:
            if name not in histories:
                raise KeyError('could not find FrHistory named=%s in %s!'%(name, path))

        seriesdict = {
            'data':seriesdict,
            't0':t0,
            'deltaT':deltaT,
            'model':histories[self._MODEL_NAME],
            'calib':histories[self._CALIB_NAME],
        }

        return [seriesdict] ### we only know how to handle a single contiguous stretch of data at the moment


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_reporters():
    return {"series:gwf": GWFSeriesReporter}
