__description__ = "a python module housing kafka-based reporters"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

from ... import hookimpl
from ... import features
from ... import utils
from . import Reporter


#-------------------------------------------------
### reporter implementations

class StreamReporter(Reporter):
    """
    a sub-class of Reporter that retrieves data (feature vectors, models, maps)
    using various streaming protocols

    this object knows how to retrieve/send data in various
    fashions (synchronous or asynchronous) given all relevant configuration options
    and a route (rootdir) where data transfer takes place

    can push relevant data to this object via report() and pull using retrieve()

    NOTE: this is just a parent class defining the API. Extensions of this class must define their own functionality

    Upon instantiation, we require a route (rootdir) as well as custom configuration options specified by keyword args
    """
    def __init__(self, *args, **kwargs):
        self._timeout = kwargs.pop('timeout', DEFAULT_STREAMREPORTER_TIMEOUT)
        Reporter.__init__(self, *args, **kwargs)

    @property
    def timeout(self):
        return self._timeout


class KafkaReporter(StreamReporter):
    """
    a sub-class of StreamReporter that reads/writes through Kafka topics.

    The rootdir argument is used as a proxy for the Kafka server that topics are hosted on,
    which is the kafka setting 'bootstrap.servers'. In addition, the 'group.id' kwarg needs to be
    specified upon instantiation. Any other kwargs can be passed in and will be used for the Kafka
    configuration to create producers and consumers at will.

    This Reporter needs to be able to connect to a Kafka instance for it to work properly.
    """
    _required_kwargs = ['group', 'port']

    def __init__(self, *args, **kwargs):
        utils.check_kafka()

        StreamReporter.__init__(self, *args, **kwargs)
        self._old = False

        # set up container for producers/consumers
        self._kafka_kwargs = self.kwargs.get('kafka_kwargs', {})
        self.consumers = {}
        self.producers = {}

    @property
    def start(self):
        """it appears I have to re-define this because inheritance doesn't take care of this for me"""
        return self._start

    @start.setter
    def start(self, start):
        t = self.timestamp ### current timestamp
        self._start = start ### update
        self._old = self.timestamp <= t ### figure out whether we're at an older timestamp than we were

    @property
    def end(self):
        """it appears I have to re-define this because inheritance doesn't take care of this for me"""
        return self._end

    @end.setter
    def end(self, end):
        t = self.timestamp ### current timestamp
        self._end = end ### update
        self._old = self.timestamp <= t ### figure out whether we're at an older timestamp than we were

    @property
    def timestamp(self):
        return 0.5*(self.start+self.end)

    @property
    def old(self):
        return self._old

    def _register(self, process_type, nickname):
        if process_type == 'producer':
            self._register_producer(nickname)
        elif process_type == 'consumer':
            self._register_consumer(nickname)
        else:
            raise ValueError("Process type can only be 'consumer' or 'producer'")

    def _register_producer(self, nickname):
        self.producers[nickname] = utils.KafkaProducer(self.kwargs['group'], self.kwargs['port'], **self._kafka_kwargs)

    def _register_consumer(self, nickname):
        self.consumers[nickname] = utils.KafkaConsumer(self.kwargs['group'], self.kwargs['port'], topics=[nickname], **self._kafka_kwargs)

    def report(self, nickname, value, preferred=False):
        """
        Creates a Kafka producer to send messages (value) to a given Kafka topic (nickname),
        with a given timestamp (timestamp).

        Uses pickle to serialize messages.
        """
        if self.old:
            raise ValueError('KafkaReporter has already reported a timestamp larger than %.3f'%self.timestamp)

        ### create producer if one not already allocated
        if nickname not in self.producers:
            self._register_producer(nickname)

        if isinstance(value, features.Dataset): ### don't transfer classifier_data reference for dataset
            for vector in value:
                vector.classifier_data = None

        ### push data to the given topic (nickname)
        ### NOTE: we take no action based on preferred here because that's handled by self.old
        self.producers[nickname].produce(timestamp=self.timestamp, topic=nickname, value=pickle.dumps(value))

        ### flush out topic of sent packets (block until message is sent)
        self.producers[nickname].flush()

        return 'kafka topic= %s from %s'%(nickname, self.kwargs['port'])

    def retrieve(self, nickname, preferred=False):
        """
        Creates a Kafka consumer to retrieve messages from a given Kafka topic (nickname).

        Uses pickle to deserialize messages.
        """
        ### create consumer if one not already allocated
        if nickname not in self.consumers:
            self._register_consumer(nickname)

            ### FIXME: why do I need to get a message first before I can seek, even when assigning to a particular partition?
            message = None
            while not message:
                message = self.consumers[nickname].poll(timeout=self.timeout)

            ### point consumer to latest buffer in topic
            self.consumers[nickname].seek_to_latest(nickname)

        if preferred: ### just consume the next buffer in the topic
            message = self.consumers[nickname].poll(timeout=self.timeout)
        else:
            self.consumers[nickname].seek_timestamp(nickname, self.timestamp) ### go to offset for this timestamp
            message = self.consumers[nickname].poll(timeout=self.timeout) ### then consume

        if not message:
            return None

        elif message.error():
            if message.error().code() == utils.kafka.KafkaError._PARTITION_EOF:
                return None
            else:
                raise message.error()

        else:
            return pickle.loads(message.value())

#-------------------------------------------------
### plugin implementations

@hookimpl
def get_reporters():
    return {"kafka": KafkaReporter}
