__description__ = "a python module housing pickle-based reporters"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import pickle

from ... import hookimpl
from . import DiskReporter

#-------------------------------------------------
### reporter implementations

class PickleReporter(DiskReporter):
    """
    pickle objects instead of just writing strings into plaintxt
    """
    _suffix = 'pkl'

    def _write(self, path, obj, **kwargs):
        """
        pickles obj into path
        """
        with open(path, 'wb') as file_obj: ### FIXME: make this a truly atomic operation
            pickle.dump(obj, file_obj)

    def _read(self, path):
        """
        unpickles obj from self.path(nickname)
        """
        with open(path, 'rb') as file_obj: ### FIXME: make this a truly atomic operation
            obj = pickle.load(file_obj)
        return obj


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_reporters():
    return {"pickle": PickleReporter}
