__description__ = "a module that houses routines to set up our loggers and standardize logging"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import logging
from logging import handlers as special_handlers

#-------------------------------------------------

DEFAULT_LOG_LEVEL = 20
DEFAULT_ROOTDIR = '.'
DEFAULT_LOGNAME = 'idq'

DEFAULT_ROTATION_CADENCE = 'D' ### used to determine when TimedRotatingFileHandlers roll over
                               ### we set the default to roll over once per day

#-------------------------------------------------
### standardizing logging

def get_logpath(rootdir, logname=DEFAULT_LOGNAME):
    return os.path.join(rootdir, logname+'.log')

def configure_logger(logger, logname, log_level=DEFAULT_LOG_LEVEL, rootdir=DEFAULT_ROOTDIR, verbose=False, rotating=None):
    '''
    standardize how we instantiate loggers
    '''
    logger.setLevel(log_level)

    # set up FileHandler for output file
    if not os.path.exists(rootdir):
        try:
            os.makedirs(rootdir)
        except OSError: ### directory already exists
            pass
    log_path = get_logpath(rootdir, logname)
    handlers = []
    if rotating:
        handlers.append(special_handlers.TimedRotatingFileHandler(log_path, when=rotating, utc=True)) ### roll over every day based on UTC time
    else:
        handlers.append(logging.FileHandler(log_path)) ### just dump everything into a single file

    # set up handler for stdout
    if verbose:
        handlers.append(logging.StreamHandler())

    # remove any existing handlers from logger to avoid repeated print statements
    for handler in logger.handlers[::-1]: ### remove in reverse order. For some reason, not all handlers are removed unless we do this
        logger.removeHandler(handler)

    # add handlers to logger
    formatter = gen_formatter(logname)
    for handler in handlers:
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    return logger

def gen_formatter(logname):
    """
    standarizes formatting for loggers
    returns an instance of logging.Formatter
    """
    return logging.Formatter(f'%(asctime)s | {logname} : %(levelname)s : %(message)s')
