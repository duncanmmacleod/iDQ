__description__ = "a module housing naming conventions wihtin IDQ. This includes both path generation and parsing, along with feature naming conventions"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os

#-------------------------------------------------

HASH_TEMPLATE = '%d_%d'
def start_end2hash(start, end):
    return HASH_TEMPLATE%(start, end)

def hash2start_end(string):
    return [int(_) for _ in string.split('_')]

#-------------------------------------------------
### utility functions for path naming that may be useful throughout

def path2start_dur(path, suffix='trg'):
    '''
    extract the start and duration from a standard LIGO file name

    >>> start, dur = path2start_dur('/path/to/file/H-GSTLAL_FEATURES-1234560000-20.h5', suffix='h5')
    >>> (start, dur)
    (1234560000, 20)

    '''
    return (int(_) for _ in path[:-len(suffix)-1].split('-')[-2:])

def path2start_end(path, suffix='trg'):
    '''
    extract the start and duration from a standard LIGO file name

    >>> path2start_end('/path/to/file/H-GSTLAL_FEATURES-1234560000-20.h5', suffix='h5')
    (1234560000, 1234560020)

    '''
    start, dur = path2start_dur(path, suffix=suffix)
    return start, start+dur

START_DUR2PATH_TEMPLATE = "%s-%d-%d.%s"
def start_dur2path(prefix, start, dur, suffix='trg'):
    return START_DUR2PATH_TEMPLATE%(prefix, start, dur, suffix)

def start_end2path(prefix, start, end, suffix='trg'):
    return start_dur2path(prefix, start, int(end)-int(start), suffix=suffix)

GLOB2PATH_TEMPLATE = "%s-*-*.%s"
def glob2path(prefix, suffix='trg'):
    return GLOB2PATH_TEMPLATE%(prefix, suffix)

#-------------------------------------------------
### utility functions for directory naming that may be useful throughout

def dirMOD1e52start_dur(directory):
    '''
    extract start and duration from directory name like
        ~/basename-${gpsMOD1e5}/
    '''
    return int(os.path.basename(os.path.dirname(os.path.join(directory,''))).split('-')[-1])*100000, 100000 ### FIXME: FRAGILE

def dirMOD1e52start_end(directory):
    '''
    extract start and end from directory name like
        ~/basename-${gpsMOD1e5}/
    '''
    start, dur = dirMOD1e52start_dur(directory)
    return start, start+dur

START2DIRMOD1E5_TEMPLATE = '%s-'
def start2dirMOD1e5_template(basename='basename', rootdir=''):
    return os.path.join(rootdir, START2DIRMOD1E5_TEMPLATE%basename+'%d')

def start2dirMOD1e5(start, basename='basename', rootdir=''):
    return start2dirMOD1e5_template(basename=basename, rootdir=rootdir)%(int(start)/100000)

def dir2start_dur(directory):
    '''
    extract start and duration from directory like
        ~/${start}_${end}/
    '''
    start, end = dir2start_end(directory)
    return start, end-start

def dir2start_end(directory):
    '''
    extract start and end from directory like
        ~/${start}_${end}/
    '''
    return tuple(int(_) for _ in os.path.basename(os.path.dirname(os.path.join(directory, ''))).split('_')) ### FIXME: FRAGILE

START_END2DIR_TEMPLATE = "%d_%d"
def start_end2dirtemplate(rootdir=''):
    return os.path.join(rootdir, START_END2DIR_TEMPLATE)

def start_end2dir(start, end, rootdir=''):
    return start_end2dirtemplate(rootdir=rootdir)%(start, end)

def start_dur2dir(start, dur, rootdir=''):
    return start_end2dir(start, start+dur, rootdir=rootdir)

def start_end2fragmented_dir(start, end, rootdir='.', basename='START'):
    return start_end2dir(start, end, rootdir=start2dirMOD1e5(start, basename=basename, rootdir=rootdir))

def start_dur2fragmented_dir(start, dur, rootdir='.', basename='START'):
    return start_end2dir(start, start+dur, rootdir=rootdir, basename=basename)

#-------------------------------------------------

KWS_CHANNEL2BASENAME_TEMPLATE = "%s.%s"
def kws_channel2filename(channel, suffix='trg'):
    return KWS_CHANNEL2BASENAME_TEMPLATE%(channel, suffix)

KWM_STRIDEBASENAME2FILENAME_TEMPLATE1 = "%s-"
KWM_STRIDEBASENAME2FILENAME_TEMPLATE2 = "-%d.%s"
def kwm_stridebasename2filename(stride=32, basename='basename', suffix='trg'):
    return '%s'.join([KWM_STRIDEBASENAME2FILENAME_TEMPLATE1%basename, KWM_STRIDEBASENAME2FILENAME_TEMPLATE2%(stride, suffix)])

#-------------------------------------------------

def basename2snax_filedir(basename):
    """
    given a basename, will return a glob-compatible path compatible
    with the standard directory structure used when producing snax-based features
    """
    return basename+'-*/'

def basename2snax_filename(basename):
    """
    given a basename, will return a glob-compatible filename
    compatible with a standard hdf5 set of features written to disk
    """
    return basename+'-*.h5'

#-------------------------------------------------

def channel2omicron_filename(instrument, channel, suffix='h5'):
    return instrument+"-"+channel+"-*-*."+suffix

#-------------------------------------------------

LOGNAME_TEMPLATE = '%s-%s'
def tag2logname(tag, task):
    return LOGNAME_TEMPLATE%(tag, task)

def tag2train_logname(tag):
    return tag2logname(tag, 'train')

def tag2calibrate_logname(tag):
    return tag2logname(tag, 'calibrate')

def tag2evaluate_logname(tag):
    return tag2logname(tag, 'evaluate')

def tag2timeseries_logname(tag):
    return tag2logname(tag, 'timeseries')

def tag2report_logname(tag):
    return tag2logname(tag, 'report')

def tag2monitor_logname(tag):
    return tag2logname(tag, 'monitor')

def tag2batch_logname(tag):
    return tag2logname(tag, 'batch')

def tag2stream_logname(tag):
    return tag2logname(tag, 'stream')

DQR_LOGNAME_TEMPLATE = 'dqr-%s'
def tag2dqr_logname(tag, graceid):
    return tag2logname(tag, DQR_LOGNAME_TEMPLATE%graceid)

#---

def tag2subdir(tag, task, rootdir=''):
    return os.path.join(rootdir, task+'-'+tag)

def tag2logdir(tag, rootdir=''):
    return tag2subdir(tag, 'log', rootdir=rootdir)

def tag2traindir(tag, rootdir=''):
    return tag2subdir(tag, 'train', rootdir=rootdir)

def tag2evaluatedir(tag, rootdir=''):
    return tag2subdir(tag, 'evaluate', rootdir=rootdir)

def tag2calibratedir(tag, rootdir=''):
    return tag2subdir(tag, 'calibrate', rootdir=rootdir)

def tag2timeseriesdir(tag, rootdir=''):
    return tag2subdir(tag, 'timeseries', rootdir=rootdir)

def tag2reportdir(tag, rootdir=''):
    return tag2subdir(tag, 'report', rootdir=rootdir)

def tag2monitordir(tag, rootdir=''):
    return tag2subdir(tag, 'monitor', rootdir=rootdir)

def tag2batchdir(tag, rootdir=''):
    return tag2subdir(tag, 'batch', rootdir=rootdir)

#---

def nickname2condorname(nickname):
    return 'condor'+nickname

def bin2batchname(bin_num):
    return 'batch-%d'%bin_num

#---

NICKNAME2TOPIC = "%s%s"
def nickname2topic(nickname, task):
    return NICKNAME2TOPIC%(nickname, task)

def nickname2train_topic(nickname):
    return nickname2topic(nickname, 'train')

def nickname2evaluate_topic(nickname):
    return nickname2topic(nickname, 'evaluate')

def nickname2calibrate_topic(nickname):
    return nickname2topic(nickname, 'calibrate')

TIMESERIES2TOPIC = '%s-%stimeseries'
def nickname2timeseries_topic(instrument, nickname):
    return TIMESERIES2TOPIC%(instrument[0], nickname)
#    return nickname2topic(nickname, 'timeseries')

#---

TAG2GROUP_TEMPLATE = '%s_%s'
def tag2group(tag, job):
    return TAG2GROUP_TEMPLATE%(tag, job)

#-------------------------------------------------

def fig2file(nickname, plot, start, dur, figtype='png'):
    return start_dur2path('_'.join([nickname, plot]), start, dur, suffix=figtype)

#-------------------------------------------------

# naming conventions for channel data products
PGLITCH = 'PGLITCH'
LOGLIKE = 'LOGLIKE'
EFF = 'EFF'
FAP = 'FAP'
RANK = 'RANK'
OK = 'OK'

def channel_name_template(ifo, nickname, freq_min, freq_max):
    return ifo+":IDQ-%s_"+nickname.upper()+"_%d_%d"%(freq_min, freq_max)
