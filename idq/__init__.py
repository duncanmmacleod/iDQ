__description__ = "exists to make idq an importable module"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

__version__ = "0.4.0" ### this could be better managed, but whatever

import pluggy

hookimpl = pluggy.HookimplMarker("iDQ")
